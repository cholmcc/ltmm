dnl -*- mode: Autoconf -*- 
dnl
dnl $Id: ltmm.m4,v 1.7 2005-06-27 11:06:37 cholm Exp $ 
dnl  
dnl  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk> 
dnl
dnl  This library is free software; you can redistribute it and/or 
dnl  modify it under the terms of the GNU Lesser General Public License 
dnl  as published by the Free Software Foundation; either version 2.1 
dnl  of the License, or (at your option) any later version. 
dnl
dnl  This library is distributed in the hope that it will be useful, 
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of 
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
dnl  Lesser General Public License for more details. 
dnl 
dnl  You should have received a copy of the GNU Lesser General Public 
dnl  License along with this library; if not, write to the Free 
dnl  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
dnl  02111-1307 USA 
dnl

dnl AM_PATH_LTMM([MINIMUM-VERSION 
dnl                   [,ACTION-IF_FOUND 
dnl                    [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AM_PATH_LTMM],
[
    AC_ARG_WITH(ltmm-prefix,
        [AC_HELP_STRING([--with-ltmm-prefix],
			[Prefix where Libtool-- is installed])],
        ltmm_prefix=$withval, ltmm_prefix="")

    if test "x${LTMM_CONFIG+set}" != xset ; then 
        if test "x$ltmm_prefix" != "x" ; then 
	    LTMM_CONFIG=$ltmm_prefix/bin/ltmm-config
	fi
    fi   

    AC_PATH_PROG(LTMM_CONFIG, ltmm-config, no)
    ltmm_min_version=ifelse([$1], ,0.1,$1)
    
    AC_MSG_CHECKING(for Libtool-- version >= $ltmm_min_version)

    ltmm_found=no    
    if test "x$LTMM_CONFIG" != "xno" ; then 
       LTMM_CPPFLAGS=`$LTMM_CONFIG --cppflags`
       
       ltmm_version=`$LTMM_CONFIG -V` 
       ltmm_vers=`echo $ltmm_version | \
         awk 'BEGIN { FS = " "; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       ltmm_regu=`echo $ltmm_min_version | \
         awk 'BEGIN { FS = " "; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       if test $ltmm_vers -ge $ltmm_regu ; then 
            ltmm_found=yes
       fi
    fi
    AC_MSG_RESULT($ltmm_found - is $ltmm_version) 
   
    if test "x$ltmm_found" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(LTMM_CPPFLAGS)
])

dnl ------------------------------------------------------------------
AC_DEFUN([LTMM_CHECK_TEMPLATE_FRIEND],
[
   AC_REQUIRE([AC_PROG_CXX])
   AC_LANG_PUSH(C++)
   AH_TEMPLATE([HAVE_TEMPLATE_FRIEND], [If friend template friend functions are supported])
   AC_MSG_CHECKING([whether C++ compiler supports template friend functions])
   AC_COMPILE_IFELSE([
struct A {};
template <typename T> void f() {}

template <typename T> struct B
{
  friend void f<T>();
  B() { f<T>(); }
};

int main()
{
  B<int> b;
  return 0;
}
], [supports_template_friend=yes], [supports_template_friend=no])
   AC_MSG_RESULT([$supports_template_friend])
   if test "x$supports_template_friend" = "xyes" ; then	
      AC_DEFINE([HAVE_TEMPLATE_FRIEND])
   fi
   AC_LANG_POP(C++)
])

dnl ------------------------------------------------------------------
AC_DEFUN([LTMM_CHECK_TEMPLATE_MEMFUNC],
[
   AC_REQUIRE([AC_PROG_CXX])
   AC_LANG_PUSH(C++)
   AH_TEMPLATE([HAVE_TEMPLATE_MEMFUNC], [If template member functions are supported])
   AC_MSG_CHECKING([whether C++ compiler supports template member functions])
   AC_COMPILE_IFELSE([
template <typename T> struct B
{
  template <typename T1>
  void f(T1 x);
  B() { float c=3.14; f(c); }
};

template <typename T>
template <typename T1>
void
B<T>::f(T1 x) { (void)x; }

int main()
{
  B<int> b;
  return 0;
}
], [supports_template_memfunc=yes], [supports_template_memfunc=no])
   AC_MSG_RESULT([$supports_template_memfunc])
   if test "x$supports_template_memfunc" = "xyes" ; then	
      AC_DEFINE([HAVE_TEMPLATE_MEMFUNC])
   fi
   AC_LANG_POP(C++)
])

dnl
dnl EOF
dnl 
