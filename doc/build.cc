//
// $Id: build.cc,v 1.2 2003-06-21 11:36:19 cholm Exp $
//
//    build.cc 
//    Copyright (C) 2002  Christian Holm <cholm@linux.HAL3000> 
//   
//    This library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later
//    version.
//   
//    This library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//   
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, write to the Free
//    Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
//    MA 02111-1307 USA
//
/** @page build Using Libtool-- in your project 

    @b Libtool-- consist entirely of declaration (or header) files.
    All you need to do, is to include those file in your project
    code.  You can copy the header files into your source code, or you
    can use them from their installed location - it's entirely up to
    you, though you should read and understand the terms of the
    @link lgpl licence@endlink 

    The included file <tt>ltmm.m4</tt> defines the @b Autoconf macro 
    <tt>AM_LTMM_PATH</tt> that you can use in your
    <tt>configure.ac</tt> file.  It defines the substitution variable
    <tt>LTMM_CPPFLAGS</tt> to contain the path to the @b Libtool--
    header files.  Put that macro in your configuration file 
    @verbatim
    ...
    AC_PROG_LIBTOOL
    AM_LTMM_PATH
    ... 
    @endverbatim 
    
    and in your <tt>Makefile.am</tt>s, do
    @verbatim 
    ...
    AM_CPPFLAGS		= $(LTMM_CPPFLAGS) 
    ...
    @endverbatim 

    Alternatively, you can use the included script
    <tt>ltmm-config</tt> to obtain the information. 
*/
#error This file is not for compilation
// 
// EOF
//
