//
// $Id: backends.cc,v 1.3 2003-06-21 11:16:42 cholm Exp $
//
//    backends.cc 
//    Copyright (C) 2002  Christian Holm <cholm@linux.HAL3000> 
//   
//    This library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later
//    version.
//   
//    This library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//   
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, write to the Free
//    Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
//    MA 02111-1307 USA
//
/** @page backends Making new backends

    The base class ltmm::backend defines the interface to a backend
    used by the loader.  A backend should implement a way to load
    dynamically loadable modules.  It can do so by overloading the
    virtual public member functions of ltmm::backend. 

    A backend is registred for use by ltmm::loader::insert_backend. 

    Iteration over registered backends is provided by the ltmm::loader
    class. 
    
 
    @see @link tests/mybackend.cc mybackend.cc @endlink 
*/
#error This file is not for compilation
// 
// EOF
//
