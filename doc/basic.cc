//
// $Id: basic.cc,v 1.4 2003-12-28 11:26:45 cholm Exp $
//
//    basic.cc 
//    Copyright (C) 2002  Christian Holm <cholm@linux.HAL3000> 
//   
//    This library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later
//    version.
//   
//    This library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//   
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, write to the Free
//    Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
//    MA 02111-1307 USA
//
/** @page basic Basic usage. 

    To use the classes, include the header file 
    @link ltmm/loader.hh ltmm/loader.hh @endlink into the application,
    and get a handle to the singleton ltmm::loader object. 

    @dontinclude tests/simple.cc 
    @skip #ifndef 
    @until ltmm::loader<>&

    Note that everything that has to do with the ltmm classes are put
    into a @c try block, as all the classes throw exceptions on
    errors.  The template argument will be dealt with in 
    @ref threads. Having created the loader instance, it's time to put
    it to work, so it is asked to get a handle (load) the library @c
    libm (the normal C math library):

    @until #endif
    
    If the ltmm::loader<>::load member function somehow fails, it
    throws and ltmm::exception that contains the appropiate error
    message.  The reason that the member function returns a reference
    rather than a value or a pointer, is to emphaise that the client
    code will always get the same handle when asking for the same
    library.  Having a handle on the library, the application can then
    get a reference to any symbol the library exports: 

    @until ltmm::symbol*

    Here the application gets a reference to the cosine function in
    the C maths library.  Note, that ltmm::handle<>::find_symbol, 
    @e always returns a new ltmm::symbol object, and it's up to the
    application to free the memory associated with the symbol.  If
    caching of symbols is desired, the application can overload the
    ltmm::handle<> class (or wrap it) to do that.  The function
    referenced by the ltmm::symbol can then straight forwardly be
    executed, and the result printed: 

    @until cout 

    Finally, any errors from the classes must be caught: 

    @until EOF
    
    @see @link tests/simple.cc simple.cc @endlink
*/
#error This file is not for compilation
// 
// EOF
//
