//
// $Id: mainpage.cc,v 1.3 2003-06-21 11:19:35 cholm Exp $
//
//    toycalc.cc 
//    Copyright (C) 2002  Christian Holm <cholm@linux.HAL3000> 
//   
//    This library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later
//    version.
//   
//    This library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//   
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, write to the Free
//    Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
//    MA 02111-1307 USA
//
/** @mainpage C++ interface to @b libltdl. 
    @image html logo.png 

    @section intro Introdution 
    
    For many kinds of applications, the ability of loading code into
    the application dynamically (at runtime) is wanted.  As C++ is
    growing in popularity, that feature should be readily avaliable to
    the C++ programmer.  This set of headers provide such features by
    wrapping the highly portable C library 
    <a href="http://www.gnu.org/software/libtool">libltdl</a>.

    @section contents Contents 
    <ul>
    <li> @ref install </li>
    <li> @ref basic </li>
    <li> @ref advanced </li>
    <li> @ref classload </li>
    <li> @ref threads </li>
    <li> @ref backends </li>
    <li> @ref build </li>
    </ul>
*/
#error This file is not for compilation
//
// EOF
//
    
