//
// $Id: advanced.cc,v 1.6 2003-12-28 11:26:45 cholm Exp $
//
//    advanced.cc
//    Copyright (C) 2002  Christian Holm <cholm@linux.HAL3000> 
//   
//    This library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later
//    version.
//   
//    This library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//   
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, write to the Free
//    Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
//    MA 02111-1307 USA
//
/** @page advanced Advanced usage. 
    @section advanced_sec Advanced usage. 

    @subsection preloaded Preloaded symbols. 

    To register the preloaded symbols (needed on platforms without
    dynamic loading), the static member function
    ltmm::loader::preloaded should be called.  It returns a map of
    the preloaded symbols, but the application needn't do anything
    with it. 

    Note, that it is important that the application is linked with 
    @b Libtools @c -dlopen option for this to work - otherwise
    there'll be a compile time invalid reference to 
    @c lt_preloaded_symbols. 

    @dontinclude super.cc 
    @skip loader<>::preloaded
    @until loader<>::preloaded

    @subsection loadpath Search path

    The search path used by the loader can be modifed by calls to 
    ltmm::loader::addto_search_path, and
    ltmm::loader::search_path.  See also ltmm::loader class
    description for more on where libraries are looked for.

    @skip loader<>::instance
    @until "Reset

    @subsection iteration Iteration on handles

    An application needn't do bookkeeping on the loaded libraries.
    The loader provides two ways of iterating over the loaded
    libraries.  One using an external functor, and one using an
    ordinary iteration mechanism. 

    The first one, using an external counter is illustrated below.
    The application needs to define a functor that has the member
    function @c operator()(ltmm::handle<>&).  For example, one could
    define a functor that simply prints the file name of the loaded
    modules: 

    @dontinclude super.cc 
    @skip struct
    @until };

    Putting it to work, means instantising it, and passing that
    instance to the ltmm::loader::foreach_handle member function:

    @skip print_modules
    @until l.foreach_handle(p);

    The other way, using a (forward) iterator is straight forward.
    Simply get an iterator pointing at the beginning, and iterate
    until the end:

    @until for

    @subsection self Opening self as a module

    The classes supports opening the application itself as if it was a
    library.  Suppose the application defined the function @c foo: 
    @dontinclude super.cc 
    @skip foo
    @until }

    Then the application can get a handle to itself, by passing the
    empty string to the ltmm::loader<>::load member function, and that
    handle can then be used to obtain references to symbols in the
    application it self. 

    @skip l.load("");
    @until foos->ptr()

    @see @link tests/super.cc   super.cc @endlink 
    @see @link tests/module1.cc module1.cc @endlink 
    @see @link tests/module2.cc module2.cc @endlink 
    
*/
#error This file is not for compilation
// 
// EOF
//
