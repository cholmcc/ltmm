//
// $Id: classload.cc,v 1.5 2003-12-28 11:26:45 cholm Exp $
//
//    classload.cc 
//    Copyright (C) 2002  Christian Holm <cholm@linux.HAL3000> 
//   
//    This library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later
//    version.
//   
//    This library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//   
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, write to the Free
//    Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
//    MA 02111-1307 USA
//
/** @page classload  Loading classes
    @section classload_sec  Loading classes

    C++ compilers @e mangle the symbols it puts in object file, so
    it's generally not possible to get the right name to look for to
    get a reference to a C++ function, not to mention a member
    function.  The application programmer must therefore employ some
    trickery for the application to load classes and class instances
    dynamically.  

    @subsection abstract Abstract Interface 

    At the heart of the solution, is to have a class hierarchy that's
    rooted in one base class, and the application will deal only with
    that interface.  The base class is abstract, and serves as the
    applications `point of recognintion':

    @dontinclude object.hh 
    @skip class object 
    @until };

    This class does not define a very interesting interface, but it
    serves to outline the idea.  A concrete library should define
    sub-class(es) of this base class, and implement the methods
    there. 

    Now, as the class member functions are not readily avaliable
    dynamically (due to mangling), and hence the constructor isn't
    either, the application should use a @b factory pattern to create
    objects of the base class:

    @skip factory 
    @until };

    This factory only defines abstract interfaces. A concrete library
    must define a sub-class, and implement the interface.  Note, that
    the above factory assumes that the object constructors does not
    take any arguments.  

    @subsection concrete Concrete implementation 

    In a concrete class library, the developer should sub-class the
    abstract base class, as well as the abstract factory.  Here's one
    example of a concrete descendent of the @c object base class 

    @dontinclude myobject.cc 
    @skip class 
    @until };

    The library should also implement the @c factory base class.
    Here's one such example 

    @skip class 
    @until };

    Now, due to mangling, the application can not create an object of
    either class directly, so instead, the library must provide a C
    function (use of @c extern @c "C" linkage specification) that
    instantices a @c factory sub-class and returns it to the caller.
    Here, the @b singleton pattern was used to insure that only one
    factory of a given kind may exist:

    @skip extern 
    @until //_

    @subsection loadingit Usage of the dynamic classes

    Usage of the dynamic classes is now quite simple.  The application
    should make a ltmm::loader instance, open the dynamic module, find
    the C function that returns the factory, and call it. 

    @dontinclude factory.cc 
    @skip using 
    @until factory*

    Having obtained an object of the concrete factory, the application
    can put it to use, by calling the @c factory::create member
    function.  That returns a new pointer to a @c object object
    pointing to an object of the concrete class.  The application can
    then use the interface defined by @c object 

    @until myobject->print();

    @subsection arbitrary Calling arbitrary member functions

    Suppose the application does not know the interface that a class
    provide @e a @e priori.  It would be nice to be able to call those
    interfaces, even if the class is completely unknown.

    First, there  should be some abstract factory class that the
    application can use. 

    @dontinclude arbitrary.hh
    @skip class arbitrary_factory
    @until public:
    @skip virtual void* call
    @until };

    This class only defines one member function.  
    @c arbitrary_factory::call takes 4 arguments, a pointer to some
    memory were some sort of object may live, the name of the class,
    the name of a member function of that class, and an argument list,
    encoded as a @c void* vector.  It returns the return value of the
    member function encoded as a @c void*.

    Now, suppose there's some class that the application would like
    to interface:
    
    @dontinclude fancy.cc 
    @skip class 
    @until };

    What the library then needs is a sub class of the 
    @c arbitrary_factory class that can interface objects of that
    kind. 

    @skip class 
    @until };

    Again, this class uses the @b singletion pattern for exactly the
    same reasons as before. 

    Here, the @c fancy_factory class implements the 
    @c arbitrary_factory::call member function, as well as having an
    internal templated member function of the same name.  The default
    instantation of the member function does nothing.  However, the
    library defines two specialisations that does something; one for
    the constructor of @c fancy:

    @skip template
    @until }
    
    and one for the member function @c foo: 

    @skip template
    @until }
    
    Note how these two templated member functions decode the memory
    address, and argument list, and encode the return value. 

    Finally, the non-templated @c arbitrary_factory::call member
    function is implemeted to call the appropiate templated member
    function, based on it's 2nd and 3rd arguments:

    @skip void*
    @until //_

    The application can then use the @c fancy_factory interface to
    create and manipulate objects of the class @c fancy 

    @dontinclude factory.cc 
    @skip fancyhandle
    @until "Return

    @see @link tests/object.hh object.hh @endlink 
    @see @link tests/myobject.cc myobject.cc @endlink 
    @see @link tests/factory.cc factory.cc @endlink 
    @see @link tests/arbitrary.hh arbitrary.hh @endlink  
    @see @link tests/fancy.cc fancy.cc @endlink 
   
*/
#error This file is not for compilation
// 
// EOF
//
