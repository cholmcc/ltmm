//
// $Id: install.cc,v 1.3 2003-06-23 06:53:20 cholm Exp $
//
//    install.cc
//    Copyright (C) 2002  Christian Holm <cholm@linux.HAL3000> 
//   
//    This library is free software; you can redistribute it and/or
//    modify it under the terms of the GNU Lesser General Public
//    License as published by the Free Software Foundation; either
//    version 2.1 of the License, or (at your option) any later
//    version.
//   
//    This library is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//   
//    You should have received a copy of the GNU Lesser General Public
//    License along with this library; if not, write to the Free
//    Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
//    MA 02111-1307 USA
//
/** @page install Installation

    To install this package, go through the well known 3-step build
    process:
    @verbatim 
    ./configure 
    make 
    make install
    @endverbatim 

    For the various options that you can pass to <tt>./configure</tt>
    please do 
    @verbatim
    ./configure --help
    @endverbatim 

    @section platform Tested platforms 

    <center>
      <table border=0 style="border-top:thin gray solid;border-left:thin
        gray solid;border-bottom:thin gray solid;border-right:thin
        gray solid;"> 
      <tr style="border-bottom:thin gray solid;">
        <th>CPU</th><th>Operating System</th><th>Compiler</th>
      </tr>
      <tr style="border-bottom:thin gray solid;">
        <td>i386</td><td>Debian GNU/Linux 3.0</td>
        <td>
          GCC 2.95<br>
          GCC 3.2<br>
          Intel C++ 7.0
        </td>
      </tr>
      <tr class=doc>
        <td>i386</td><td>Windos XP (cygwin)</td>
        <td>
          GCC 2.95<br>
          GCC 3.2<br>
        </td>
      </tr>
      <tr>
        <td>i386</td><td>Windos XP</td><td>Visual C++ 6.0</td>
      </tr>
      <tr>
        <td>i386</td><td>Windos XP</td><td>Visual C++ 6.0</td>
      </tr>
    </table>
    </center>

    @section msvc Microsoft Visual C++ 
    
    To install using Microsoft Visual C++<sup>TM</sup>, you can do it
    two ways: under <a href="http://www.cygwin.com">CygWin</a> or from
    Microsoft Visual Studio<sup>TM</sup> (currently not supported).  I
    heartly recommend the Cygwin approach, as it will install
    everything for you. 

    @b Cygwin: In a @b bash shell do 
    @verbatim 
    ./configure CC=`pwd`/ide/cl					\
	        CXX=`pwd`/ide/cl				\
	        CXXFLAGS="-GX"					\
	        CPPFLAGS="-D__WINDOWS__ -D__STDC__ -DR_OK=04"
    @endverbatim 
    <ul>
      <li> @c ide/cl is a wrapper script around the Microsoft Visual
        C++ compiler @c cl.exe.  The @e reason @e d'etre of this
	wrapper is that I use @c .cc to denote source files, so I need
	to pass the option @c -Tp for each source file. </li>
      <li> @c CXXFLAGS="-GX" is to turn on exception handling in 
        Microsoft Visual C++</li>
      <li> <tt>CPPFLAGS="-D__WINDOWS__ -D__STDC__ -DR_OK=04"</tt> is
        to give some definitions we need for the nested library
        <tt>libltdl</tt>.  For some reason Microsoft Visual C++ does
	not define <tt>__WINDOWS__</tt> or similar, and likewise for 
	<tt>__STDC__</tt>.  <tt>R_OK</tt> is because the Microsoft
	system header file for @c access does not define symbolic
	names for the various flags one can pass to @c access </li>
    </ul>

    <b>Microsoft Visual Studio<sup>TM</sup>:</b> You can either open
    the workspace file <tt>ide@\ltmm.dsw</tt> from the GUI, or in a
    command prompt run 
    @verbatim
    MSDEV ide\ltmm.dsw /MAKE 
    @endverbatim 

    Note, that Microsoft Visual C++, version 6 and earlier does not
    fully support template member functions of template classes, so
    the member function ltmm::loader<ThreadPolicy>::foreach_handle
    isn't defined on that platform. 
	
*/
#error This file is not for compilation
// 
// EOF
//
