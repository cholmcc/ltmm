//
// $Id: examples.cc,v 1.7 2003-12-28 10:53:30 cholm Exp $
//
//      examples.cc 
//      Copyright (C) 2003  Christian Holm <cholm@linux.HAL3000> 
//   
//      This library is free software; you can redistribute it and/or 
//      modify it under the terms of the GNU Lesser General Public 
//      License as published by the Free Software Foundation; either 
//      version 2.1 of the License, or (at your option) any later version. 
//   
//      This library is distributed in the hope that it will be useful, 
//      but WITHOUT ANY WARRANTY; without even the implied warranty of 
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//      Lesser General Public License for more details. 
//   
//      You should have received a copy of the GNU Lesser General Public 
//      License along with this library; if not, write to the Free Software 
//      Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//      02111-1307  USA  
//
/** @file   examples.cc
    @author Christian Holm
    @date   Thu Jan 30 10:56:10 2003
    @brief  Example documentation */

/** @page examples Examples
 */
/** @example tests/module1.cc 
    @par A simple module with a function. 
    This is used by the main.cc example. */

/** @example tests/module2.cc 
    @par A simple module with a function. 
    This is used by the main.cc example. */

/** @example tests/simple.cc 
    @par A very simple example of how to use the classes.
    This just opens @c libm.so, and finds the symbol @c cos, and
    executes it with an argument of 0, and prints the result.  Not
    very interesting, but it surfice to show the main functionality of
    the classes. */

/** @example tests/super.cc 
    @par Test of all basic features. 
    This example is a test of all the basic features of @b Libtool--.
    It loads two modules module1.cc and module2.cc and calls a
    function (with C linkage) from module1.cc. It also uses
    ltmm::loader<Lock>::foreach and
    ltmm::loader<Lock>::iterator to list the loaded modules. */ 

/** @example tests/object.hh 
    @par Abstract Base Classes for factory.cc example. 
    This header defines two ABCs for the factory.cc example: the class
    @c object that is the root of the hierarchy, and @c factory that
    is an abstract create of concrete sub-class of the @c object
    class. */

/** @example tests/myobject.cc
    @par Concrete @c object and @c factory classes. 
    This defines two concrete classes used by the factory.cc example:
    The clas @c myobject which subclasses @c object, and @c myfactory
    which subclasses factory.  The latter creates objects of the
    former.  Also defined is the function @c myfactory_init (with C
    linkage) that the factory.cc example looks for in the loaded
    module.  It creates a (singleton) object of the @c myfactory
    class. */ 

/** @example tests/factory.cc
    @par Example of using a factory to create objects. 
    This example uses the interface of @c object, and @c factory to
    dynamically create objects of usre defined subclasses of 
    @c object.  The example loads the module from myobject.cc, creates
    a @c myfactory object, and then use that object to create a
    concrete @c object (@c myobject), and calls the member functon 
    @c print on it.  It shows how to dynamically load classes from a
    dynamically loaded library.  Also shown, is how to call an
    arbitrary member function of the concrete classes via the factory
    interface.  Of course, the coolest thing would be if the factory
    could be generated automatically - however, for that to happen,
    one need a full C++ parser which is outside the scope of the
    package. */

/** @example tests/arbitrary.hh
    @par Abstract Base Class Factory to interface an arbitrary class. 
    This class provides the interface for calling an arbitrary member
    function of an arbitriary class.  The concrete implementations
    must devise a way to call the actual member functions. 
    @see @link tests/fancy.cc fancy.cc @endlink 
*/

/** @example tests/fancy.cc
    @par A class without hierarchy root, and it's factory interface. 
    This provides a concrete implementation of an 
    @c arbitrary_factory.  It uses template member functions to
    resolve the member function calls. 
*/

/** @example tests/mybackend.cc 
    @par Example of custom backend module loader.  This shows how to
    use the custom backend layer for module loaders.  It tests all of
    the features of the backend part, including iteration, and getting
    a reference to ltmm::internal_backend<ThreadPolicy> object for the
    build in module loaders defined by libltdl. */
#error This file is not for compilation
//
// EOF
//
