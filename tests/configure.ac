dnl
dnl $Id: configure.ac,v 1.1 2003-12-28 10:53:30 cholm Exp $
dnl
dnl   General C++ parser and lexer
dnl   Copyright (C) 2002  Christian Holm Christensen <cholm@nbi.dk>
dnl
dnl   This library is free software; you can redistribute it and/or
dnl   modify it under the terms of the GNU Lesser General Public License 
dnl   as published by the Free Software Foundation; either version 2 of 
dnl   the License, or (at your option) any later version.  
dnl
dnl   This library is distributed in the hope that it will be useful,
dnl   but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
dnl   Lesser General Public License for more details. 
dnl
dnl   You should have received a copy of the GNU Lesser General Public
dnl   License along with this library; if not, write to the Free
dnl   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
dnl   MA 02111-1307  USA  
dnl
dnl AM_PATH_LTMM([MINIMUM-VERSION 
dnl                   [,ACTION-IF_FOUND 
dnl                    [, ACTION-IF-NOT-FOUND]])
AC_DEFUN([AM_PATH_LTMM],
[
    AC_ARG_WITH(ltmm-prefix,
        [  --with-ltmm-prefix      Prefix where Libtool-- is installed],
        ltmm_prefix=$withval, ltmm_prefix="")

    if test "x${LTMM_CONFIG+set}" != xset ; then 
        if test "x$ltmm_prefix" != "x" ; then 
	    LTMM_CONFIG=$ltmm_prefix/bin/ltmm-config
	fi
    fi   

    AC_PATH_PROG(LTMM_CONFIG, ltmm-config, no)
    ltmm_min_version=ifelse([$1], ,0.1,$1)
    
    AC_MSG_CHECKING(for Libtool-- version >= $ltmm_min_version)

    ltmm_found=no    
    if test "x$LTMM_CONFIG" != "xno" ; then 
       LTMM_CPPFLAGS=`$LTMM_CONFIG --cppflags`
       
       ltmm_version=`$LTMM_CONFIG -V` 
       ltmm_vers=`echo $ltmm_version | \
         awk 'BEGIN { FS = " "; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       ltmm_regu=`echo $ltmm_min_version | \
         awk 'BEGIN { FS = " "; } \
	   { printf "%d", ($''1 * 1000 + $''2) * 1000 + $''3;}'`
       if test $ltmm_vers -ge $ltmm_regu ; then 
            ltmm_found=yes
       fi
    fi
    AC_MSG_RESULT($ltmm_found - is $ltmm_version) 
   
    if test "x$ltmm_found" = "xyes" ; then 
        ifelse([$2], , :, [$2])
    else 
        ifelse([$3], , :, [$3])
    fi
    AC_SUBST(LTMM_CPPFLAGS)
])

dnl
dnl EOF
dnl 


dnl ------------------------------------------------------------------
dnl Autoconf 2.53 requires a lot of stuff here
AC_INIT([Libtool-- tests], [1.2], [cholm@nbi.dk], [ltmm-tests])
AC_PREREQ([2.53])
AC_COPYRIGHT([GNU Lesser General Public License])
AC_REVISION($Revision: 1.1 $)
AC_CONFIG_SRCDIR([super.cc])
AM_INIT_AUTOMAKE([$PACKAGE_TARNAME], [$PACKAGE_VERSION])
AC_PROG_CC
AC_PROG_CXX
case $CC in 
*ecc|*icc) 
   dnl
   dnl Using Intel C++ compiler, we need to do some tricks to 
   dnl fool libtool.  What we do, is to set some variables
   dnl before the checks, so that the cache will think them set 
   dnl already 
   AC_CACHE_VAL([lt_cv_prog_cc_pic],[lt_cv_prog_cc_pic=-fPIC])
   AC_CACHE_VAL([lt_cv_prog_cc_wl],[lt_cv_prog_cc_wl='-Wl,'])
   AC_CACHE_VAL([lt_cv_archive_cmds_need_lc],[lt_cv_archive_cmds_need_lc=no]) 
   ;; 
esac
AC_LIBLTDL_INSTALLABLE
AC_SUBST(INCLTDL)
AC_SUBST(LIBLTDL)
AC_LIBTOOL_DLOPEN
AC_PROG_LIBTOOL
AM_PATH_LTMM
AC_CONFIG_FILES([Makefile])
AC_OUTPUT

dnl
dnl EOF
dnl
