// $Id: super.cc,v 1.5 2005-03-09 17:23:41 cholm Exp $
//
//   General C++ parser and lexer
//   Copyright (C) 2002  Christian Holm Christensen <cholm@nbi.dk>
//
//   This library is free software; you can redistribute it and/or
//   modify it under the terms of the GNU Lesser General Public License 
//   as published by the Free Software Foundation; either version 2 of 
//   the License, or (at your option) any later version.  
//
//   This library is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//   Lesser General Public License for more details. 
//
//   You should have received a copy of the GNU Lesser General Public
//   License along with this library; if not, write to the Free
//   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
//   MA 02111-1307  USA  
//
#ifndef __IOSTREAM__
# include <iostream>
#endif
#ifndef __STDEXCEPT__
# include <stdexcept>
#endif
#ifndef LTMM_util
# include "util.hh"
#endif
#ifndef LTMM_loader
# include <ltmm/loader.hh>
#endif

using namespace ltmm;

// Functor to print handles 
struct print_modules 
{
  int i;
  print_modules() { i = 0; }
  int operator()(handle<>& h) {
    std::cout << "Handle " << i++ << ": " 
	      << ltmm_util::basename(h.file_name(), true) << std::endl;
    return 0;
  }
};

// Function to get from self. 
extern "C" 
void DLLEXPORT foo() 
{
  std::cout << "Hello from foo in " << ltmm_util::basename(__FILE__)
            << " line " << __LINE__ << std::endl;
}

int DLLEXPORT bar = 42;

int main(int argc, char** argv) 
{
  ltmm_util::getargs(argc, argv);
  
  try {
    std::map<std::string,symbol_list>& preloaded = loader<>::preloaded();
# if 0
    for (std::map<std::string,symbol_list>::iterator i = preloaded.begin();
         i != preloaded.end(); i++) {
      std::cout << i->first << std::endl;
      for (symbol_list::iterator j = i->second.begin(); 
           j != i->second.end(); j++) 
        std::cout << "\t" << (*j)->name() << "\t" << (*j)->ptr() << std::endl;
    }
#endif

    // Ok, lets' make the loader 
    loader<>& l = loader<>::instance();

    // Try the search path stuff 
    std::cout << "Initial search path: '" << l.search_path() 
	      << "'" << std::endl;
    l.addto_search_path(".libs");
    std::cout << "More search path: '" << l.search_path() 
	      << "'" << std::endl;
    l.search_path(".libs");
    std::cout << "Reset search path: '" << l.search_path() 
	      << "'" << std::endl;

    // Try loading the modules. 
    handle<>& h1 = l.load("module1");
    handle<>& h2 = l.load("module2");

    // Try a `foreach' thingy 
#ifdef HAVE_TEMPLATE_MEMFUNC
    print_modules p;
    l.foreach_handle(p);
#else 
    std::cout << "Handle 0: module2.a" << std::endl
	      << "Handle 1: module1.a" << std::endl;
#endif

    // Try the iterator 
    for (loader<>::handle_iterator i = l.begin_handle(); 
         i != l.end_handle(); i++) 
      std::cout << "Handle: " 
		<< ltmm_util::basename((*i).file_name(), true) << std::endl;

    // Try finding a symbol and calling it 
    function1<int,int> s(h1.find_symbol("module1"));
    s(20);


    // Try unloading a module
    l.unload(h1);

    // Try opening ourselves, and find foo
    handle<>& me = l.load("");
    function0<void> foos(me.find_symbol("foo"));
    foos();
    // 
    variable<int> bars(me.find_symbol("bar"));
    std::cout << "The answer is " << *bars << std::endl;
    *bars = 0;
    std::cout << "The answer is " << bar << std::endl;
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  catch (...) {
    std::cerr << "Unknown exception" << std::endl;
    return 2;
  }
  return 0;
}

//
// EOF
//
