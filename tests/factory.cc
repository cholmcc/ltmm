//
// $Id: factory.cc,v 1.7 2005-01-12 18:17:48 cholm Exp $
//
//      factory.cc 
//      Copyright (C) 2003  Christian Holm <cholm@linux.HAL3000> 
//   
//      This library is free software; you can redistribute it and/or 
//      modify it under the terms of the GNU Lesser General Public 
//      License as published by the Free Software Foundation; either 
//      version 2.1 of the License, or (at your option) any later version. 
//   
//      This library is distributed in the hope that it will be useful, 
//      but WITHOUT ANY WARRANTY; without even the implied warranty of 
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//      Lesser General Public License for more details. 
//   
//      You should have received a copy of the GNU Lesser General Public 
//      License along with this library; if not, write to the Free Software 
//      Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//      02111-1307  USA  
//
/** @file   factory.cc
    @author Christian Holm
    @date   Thu Jan 30 10:28:51 2003
    @brief   */

#ifndef __IOSTREAM__
#include <iostream>
#endif
#ifndef __STDEXCEPT__
#include <stdexcept>
#endif
#ifndef LTMM_loader
#include <ltmm/loader.hh>
#endif
#ifndef __object__
#include "object.hh"
#endif
#ifndef __arbitrary__
#include "arbitrary.hh"
#endif

using namespace ltmm;

typedef function0<factory*> factory_init;
typedef function0<arbitrary_factory*> arbitrary_init;

int main(int argc, char** argv) 
{
  try {
    std::map<std::string,symbol_list>& preloaded = loader<>::preloaded();
    loader<>& l              = loader<>::instance();
    l.addto_search_path(".libs");

    handle<>&      myhandle  = l.load("myobject.la");
    factory_init   myinit(myhandle.find_symbol("myfactory_init"));
    factory*       myfactory = myinit();
    object*        myobject  = myfactory->create();
    myobject->print();

    // Load a module 
    handle<>&         fancyhandle = l.load("fancy.la");
    arbitrary_init    fancyinit(fancyhandle.find_symbol("fancy_factory_init"));
    arbitrary_factory* fancyfactory = fancyinit();
    
    std::vector<void*> args;
    void* ret = 0;
    void* fancyobject = fancyfactory->call(0, "fancy", "fancy", args);
    
    int a = 10;
    int b = 20;
    int c;
    args.push_back(&a);
    args.push_back(&b);
    ret = fancyfactory->call(fancyobject, "fancy", "foo", args);
    c = *((int*)ret);
    std::cout << "Return value: " << c << std::endl;
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  return 0;
}

//
// EOF
//
