//
// $Id: util.hh,v 1.7 2005-03-09 17:23:41 cholm Exp $
//
//  util.hh 
//  Copyright (C) 2003  Christian Holm <cholm@linux.HAL3000> 
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public License
//  as published by the Free Software Foundation; either version 2.1
//  of the License, or (at your option) any later version.
// 
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307 USA
//
#ifndef LTMM_util
#define LTMM_util
#ifndef __STRING__
# include <string>
#endif
#ifdef HAVE_CONFIG_H
# include "config.hh"
#endif
#ifndef __WINDOWS__
# define DIR_SEPS "/"
#else 
# define DIR_SEPS "/\\"
#endif
#ifdef __WINDOWS__
# define DLLEXPORT __declspec(dllexport)
#else
# define DLLEXPORT
#endif

namespace ltmm_util 
{
  inline
  std::string basename(const std::string& path, bool sans_ext=false) 
  {
    std::string::size_type slash = path.find_last_of(DIR_SEPS);
    std::string tmp(path);
    if (slash != std::string::npos) 
      tmp = std::string(path, slash+1, path.size()-slash);
    if (sans_ext) {
      std::string::size_type period = tmp.find_last_of(".");
      if (period != std::string::npos) {
	tmp = std::string(tmp, 0, period);
      }
    }
    return tmp;
  }

  void usage(const std::string& progname) 
  {
    std::cout << "Usage: " << progname << " [OPTIONS]\n\n" 
	      << "Options:\n" 
	      << "\t-h, --help\tThis help\n"
	      << "\t    --version\tShow version number\n"
	      << std::endl;
    exit(0);
  }

  void version() 
  {
    std::cout << PACKAGE_VERSION << std::endl;
    exit (0);
  }
  
  void unknown(const std::string& progname, const char* opt) 
  {
    std::cerr << progname << ": Unknown option \"" << opt << "\"" 
	      << std::endl;
    exit(1);
  }
  
  void getargs(int argc, char** argv) 
  {
    std::string progname(basename(argv[0]));
    
    for (int i = 1; i < argc; i++) {
      if (argv[i] && argv[i][0] == '-') {
	if (argv[i][1] == '-') {
	  std::string arg(argv[i]);
	  if (arg == "--version")   version();
	  else if (arg == "--help") usage(progname);
	  else                      unknown(progname, argv[i]);
	}
	switch(argv[i][1]) {
	case 'h': usage(progname) ;           break;
	case 'V': version()       ;           break;
	default:  unknown(progname, argv[i]); break;
	}
      }
    }
  }
}

#endif
// 
// EOF
//
