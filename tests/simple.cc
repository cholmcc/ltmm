// $Id: simple.cc,v 1.4 2005-01-12 18:17:49 cholm Exp $
//
//   General C++ parser and lexer
//   Copyright (C) 2002  Christian Holm Christensen <cholm@nbi.dk>
//
//   This library is free software; you can redistribute it and/or
//   modify it under the terms of the GNU Lesser General Public License 
//   as published by the Free Software Foundation; either version 2 of 
//   the License, or (at your option) any later version.  
//
//   This library is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//   Lesser General Public License for more details. 
//
//   You should have received a copy of the GNU Lesser General Public
//   License along with this library; if not, write to the Free
//   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
//   MA 02111-1307  USA  
//
#ifndef __IOSTREAM__
#include <iostream>
#endif
#ifndef __STDEXCEPT__
#include <stdexcept>
#endif
#ifndef LTMM_loader
#include <ltmm/loader.hh>
#endif
#ifdef __CYGWIN__
double cos(double x) { return 1; }
#endif

int main() 
{
  try {
    std::map<std::string,ltmm::symbol_list>& preloaded =
      ltmm::loader<>::preloaded();
    ltmm::loader<>& loader = ltmm::loader<>::instance();    
#if defined(_MSC_VER) || defined(__CYGWIN__)
    ltmm::handle<>& handle = loader.load("");
#else
    ltmm::handle<>& handle = loader.load("libm.so");
#endif
    ltmm::symbol*   symbol = handle.find_symbol("cos");
    ltmm::function1<double,double> cos_func(symbol);
    double result          = cos_func(0);
    std::cout << "cos(0) = " << result << std::endl;
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  return 0;
}

//
// EOF
//
