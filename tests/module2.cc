//
// $Id: module2.cc,v 1.6 2005-03-09 17:23:41 cholm Exp $
//
//  module2.cc 
//  Copyright (C) 2003  Christian Holm <cholm@linux.HAL3000> 
//
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public 
//  License as published by the Free Software Foundation; either 
//  version 2.1 of the License, or (at your option) any later version. 
//
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
//
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free Software 
//  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
//  02111-1307  USA  
#ifndef __IOSTREAM__
#include <iostream>
#endif
#ifndef LTMM_util
#include "util.hh"
#endif

extern "C" {
  int DLLEXPORT module2(int a) 
  {
    std::cout << "Hello from module2 in " << ltmm_util::basename(__FILE__)
	      << " line " << __LINE__ << ":\t" << a << std::endl;
    return 0; 
  }
}

//
// EOF
//
