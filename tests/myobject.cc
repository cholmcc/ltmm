//
// $Id: myobject.cc,v 1.8 2005-03-09 17:23:41 cholm Exp $ 
//  
//  myobject
//  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk> 
//
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public License 
//  as published by the Free Software Foundation; either version 2.1 
//  of the License, or (at your option) any later version. 
//
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free 
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
//  02111-1307 USA 
//
/** @file   myobject.cc
    @author Christian Holm
    @date   Thu Jan 30 10:21:36 2003
    @brief  Concrete object */
#ifndef __object__
#include "object.hh"
#endif
#ifndef __STRING__
#include <string>
#endif
#ifndef __IOSTREAM__
#include <iostream>
#endif
#ifndef LTMM_util
#include "util.hh"
#endif

//____________________________________________________________________
class myobject : public object
{
public:
  myobject() {}
  virtual ~myobject() {}
  void print() const {
    std::cout << "Hello from myobject " << ltmm_util::basename(__FILE__) 
	      << " line "  << __LINE__ << std::endl;
  }
};

//____________________________________________________________________
class myfactory : public factory 
{
private:
  static factory* _instance;
  myfactory() {}
  virtual ~myfactory() {}  
public:
  static factory* instance() {
    if (!_instance) _instance = new myfactory;
    return _instance;
  }
  object* create(){ return new myobject; }
};

//____________________________________________________________________
factory* myfactory::_instance = 0;

//____________________________________________________________________
extern "C" 
{
  factory DLLEXPORT *myfactory_init() 
  {
    return myfactory::instance();
  }
}

//____________________________________________________________________
//
// EOF
//
