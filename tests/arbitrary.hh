//
// $Id: arbitrary.hh,v 1.2 2003-05-13 14:19:52 cholm Exp $ 
//  
//  arbitrary
//  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk> 
//
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public License 
//  as published by the Free Software Foundation; either version 2.1 
//  of the License, or (at your option) any later version. 
//
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free 
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
//  02111-1307 USA 
//
#ifndef __arbitrary__
#define __arbitrary__
#ifndef __STRING__
#include <string>
#endif
#ifndef __VECTOR__
#include <vector>
#endif

/** @file   arbitrary.hh
    @author Christian Holm
    @date   Thu Jan 30 10:17:15 2003
    @brief  Abstract base class for factories. */

/** @brief Factory to call an arbitriary method of classes. */
class arbitrary_factory 
{
public:
  /** @brief Call a member function of a class. 
      @param o The object to call the member function of. 
      @param c The class name as of the object @a o as a string.
      @param m A string giving the name of the member function. 
      @param a A vector of arguments to pass to the member function.
      @return the address of the result of the member function, if
      any, otherwise 0. */
  virtual void* call(void* o, const std::string& c, 
		     const std::string& m, std::vector<void*>& a) = 0;
};


#endif
//____________________________________________________________________
//
// EOF
//
