//
// $Id: mybackend.cc,v 1.8 2005-03-09 17:23:41 cholm Exp $ 
//  
//  mybackend
//  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk> 
//
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public License 
//  as published by the Free Software Foundation; either version 2.1 
//  of the License, or (at your option) any later version. 
//
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free 
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
//  02111-1307 USA 
//
#ifndef __IOSTREAM__
# include <iostream>
#endif
#ifndef __STDEXCEPT__
# include <stdexcept>
#endif
#ifndef __VECTOR__
# include <vector>
#endif
#ifndef LTMM_util
# include "util.hh"
#endif
#ifndef LTMM_loader
# include <ltmm/loader.hh>
#endif

/** @file   tests/mybackend.cc
    @author Christian Holm
    @date   Fri Jan 31 05:58:42 2003
    @brief  A backend */

using namespace ltmm;

#ifdef HAVE_DLFCN_H 
# ifndef _DLFCN_H
#  include <dlfcn.h>
# endif
/** User defined module loader, using a @c dlopen interface */
struct mybackend : public backend<>
{
  /** Constructor */
  mybackend() : backend<>("mydl", "") {}
  /** Load a module 
      @param name The module to load 
      @exception ltmm::backend_error if module couldn't be loaded
      @return handle to module on success, 0 otherwise */
  void* load(const std::string& name) throw(backend_error) { 
    lt_module module = 0;
    if (!(module = dlopen (name.c_str(), RTLD_GLOBAL|RTLD_LAZY|RTLD_NOW)))
      throw backend_error(dlerror());
    return module;
  }
  /** Unload a module 
      @param m Module handle 
      @exception ltmm::backend_error if module couldn't be unloaded
      @return true on success */
  bool unload(void* m) throw(backend_error) {
    if (dlclose(m) != 0) throw backend_error(dlerror());
    return true;
  }
  /** Find a symbol in a module 
      @param m    The module to find the symbol @a name in 
      @exception ltmm::backend_error if symbol couldn't be found
      @param name The name of the symbol to find
      @return A handle to the symbol is found */
  void* find_symbol(void* m, const std::string& name) throw(backend_error) {
    lt_ptr address = 0;
    if (!(address = dlsym(m, name.c_str()))) throw backend_error(dlerror());
    return address;
  }
  /** Exit handler 
      @return true always */
  bool exit() throw(backend_error) { return true; }
};
#elif __WINDOWS__
#include <windows.h>
/** User defined module loader, using a @c LoadLibrary interface */
struct mybackend : public backend<single_thread>
{
  /** Constructor */
  mybackend() : backend<>("mydl", "") {}
  /** Load a module 
      @param name The module to load 
      @exception ltmm::backend_error if module couldn't be loaded
      @return handle to module on success, 0 otherwise */
  void* load(const std::string& name) throw(backend_error) { 
    lt_module module = 0;
    if (!(module = LoadLibrary(name.c_str())))
      throw backend_error("Can not open");
    return module;
  }
  /** Unload a module 
      @param m Module handle 
      @exception ltmm::backend_error if module couldn't be unloaded
      @return true on success */
  bool unload(void* m) throw(backend_error) {
    HINSTANCE h = (HINSTANCE)m;
    if (FreeLibrary(h) != 0) throw backend_error(GetLastError());
    return true;
  }
  /** Find a symbol in a module 
      @param m    The module to find the symbol @a name in 
      @exception ltmm::backend_error if symbol couldn't be found
      @param name The name of the symbol to find
      @return A handle to the symbol is found */
  void* find_symbol(void* m, const std::string& name) throw(backend_error) {
    lt_ptr address = 0;
    HINSTANCE h = (HINSTANCE)m;
    if (!(address = GetProcAddress(h, name.c_str()))) 
      throw backend_error(GetLastError());
    return address;
  }
  /** Exit handler 
      @return true always */
  bool exit() throw(backend_error) { return true; }
};
#else
/** User defined module loader, dummy implementation */
struct mybackend : public backend<>
{
  /** Constructor */
  mybackend() : backend<>("mydl", "") {}
  /** Load a module 
      @param name The module to load 
      @return 0 always */
  void* load(const std::string& name) throw(backend_error) { return 0; }
  /** Unload a module 
      @param m Module handle 
      @return false always */
  bool unload(void* m) throw(backend_error) { return false; }
  /** Find a symbol in a module 
      @param m    The module to find the symbol @a name in 
      @param name The name of the symbol to find
      @return 0 always */
  void* find_symbol(void* m, const std::string& name) throw(backend_error) {
    return 0;
  }
  /** Exit handler 
      @return true always */
  bool exit() throw(backend_error) { return true; }
};
#endif

extern "C" {
  /** Function to print some information */
  void DLLEXPORT foo() 
  {
    std::cout << "Hello form foo in " << ltmm_util::basename(__FILE__)
	      << " line " << __LINE__ << std::endl;
  }
}

int main(int argc, char** argv) 
{
  try {
    std::map<std::string,symbol_list>& preloaded = loader<>::preloaded();
    // Ok, lets' make the loader 
    loader<>& l = loader<>::instance();
    symbol s((void*)&foo, "foo");
    l.preload(s);

    // Make a backend instance; 
    mybackend my;
    l.insert_backend(my);
    
    // Print list of loaders 
    loader<>::backend_iterator i = l.find_backend("mydl");
    if (i == l.end_backend())  
      throw std::runtime_error("didn't find the backend");
    std::cout << "My backend: " << (*i).name() << std::endl;

    // Add .libs to the search path
    l.addto_search_path(".libs");
    // Load a module
    handle<>& h = l.load("module1.la");

    // Open self. 
    handle<>& self = l.load("");

    // Find preloaded symbol and execute it
    symbol* pre = self.find_symbol("foo");
    ((void(*)())(pre->ptr()))();
  }
  catch (std::exception& e) {
    std::cerr << e.what() << std::endl;
    return 1;
  }
  catch (...) {
    std::cerr << "unknown exception" << std::endl;
    return 2;
  }
  return 0;
}

//____________________________________________________________________
//
// EOF
//
