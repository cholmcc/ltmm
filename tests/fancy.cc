//
// $Id: fancy.cc,v 1.8 2005-03-09 17:23:41 cholm Exp $ 
//  
//  myobject
//  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk> 
//
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public License 
//  as published by the Free Software Foundation; either version 2.1 
//  of the License, or (at your option) any later version. 
//
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free 
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
//  02111-1307 USA 
//
/** @file   fancy.cc
    @author Christian Holm
    @date   Thu Jan 30 10:21:36 2003
    @brief  Concrete object */
#ifndef __arbitrary__
#include "arbitrary.hh"
#endif
#ifndef __IOSTREAM__
#include <iostream>
#endif
#ifndef LTMM_util
#include "util.hh"
#endif

//____________________________________________________________________
class fancy 
{
private:
public:
  fancy() {}
  ~fancy() {}
  int foo(int a, int b) {
    std::cout << "Hello from fancy::foo in " << ltmm_util::basename(__FILE__)
	      << " line " << __LINE__
	      << ": a=" << a << " b=" << b << std::endl;
    return a+b;
  }
};

//____________________________________________________________________
class fancy_factory : public arbitrary_factory 
{
  static arbitrary_factory* _instance;
  fancy_factory() {}
  virtual ~fancy_factory() {}  
public:
  static arbitrary_factory* instance(); 
  void* call(void* o, const std::string& c, const std::string& m, 
	     std::vector<void*>& a);
#ifdef HAVE_TEMPLATE_MEMFUNC
  template<int c, int m> void* callit(void*,std::vector<void*>& a);
#endif
};

//____________________________________________________________________
arbitrary_factory* fancy_factory::_instance = 0;

//____________________________________________________________________
arbitrary_factory* fancy_factory::instance()
{
  if (!_instance) _instance = new fancy_factory;
  return _instance;
}

#ifdef HAVE_TEMPLATE_MEMFUNC
# define FANCY_FACTORY fancy_factory::
#else 
# define FANCY_FACTORY
#endif

//____________________________________________________________________
template<int c, int m> 
void* FANCY_FACTORY callit(void*,std::vector<void*>& a) 
{
  return 0;
}

//____________________________________________________________________
template <>
void* FANCY_FACTORY callit<1,1>(void* o, std::vector<void*>& a) 
{
  fancy* m = new fancy;
  return m;
}
//____________________________________________________________________
template <>
void* FANCY_FACTORY callit<1,2>(void* o, std::vector<void*>& a) 
{
  static int retval;
  if (!o) return 0;
  int aa = *((int*)a[0]);
  int bb = *((int*)a[1]);
  fancy* m = static_cast<fancy*>(o);
  retval = m->foo(aa, bb);
  return static_cast<void*>(&retval);
}
//____________________________________________________________________
void* fancy_factory::call(void* o, const std::string& c, 
			  const std::string& m, std::vector<void*>& a) 
{
  if (c == "fancy") { 
    if (m == "foo")   return callit<1, 2>(o, a);
    if (m == "fancy") return callit<1, 1>(o, a);
  }
  return 0;
}

//____________________________________________________________________
extern "C" {
  arbitrary_factory DLLEXPORT * fancy_factory_init() 
  {
    return fancy_factory::instance();
  }
}

//____________________________________________________________________
//
// EOF
//

  

  
  
