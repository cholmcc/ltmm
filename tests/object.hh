//
// $Id: object.hh,v 1.4 2005-01-11 18:00:12 cholm Exp $ 
//  
//  object
//  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk> 
//
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public License 
//  as published by the Free Software Foundation; either version 2.1 
//  of the License, or (at your option) any later version. 
//
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free 
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
//  02111-1307 USA 
//
#ifndef __object__
#define __object__
#ifndef __STRING__
#include <string>
#endif

/** @file   object.hh
    @author Christian Holm
    @date   Thu Jan 30 10:17:15 2003
    @brief  Abstract base class for factories. */

/** Abstract base class for objects created by the factory */
class object 
{
public:
  object() {}
  virtual ~object() {}
  virtual void print() const = 0;
};

/** Abstract base class for factories. */
class factory
{
public:
  /** Constructor. */
  factory() {}
  /** Destructor. */
  virtual ~factory() {}
  /** Create an object, returning the address. */
  virtual object* create() = 0;
};


#endif
//____________________________________________________________________
//
// EOF
//
