//
// $Id: backend.hh,v 1.6 2005-01-12 18:17:48 cholm Exp $ 
//  
//  ltmm::backend
//  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk> 
//
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public License 
//  as published by the Free Software Foundation; either version 2.1 
//  of the License, or (at your option) any later version. 
//
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free 
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
//  02111-1307 USA 
//
#ifndef LTMM_backend
#define LTMM_backend
#ifndef __STRING__
#include <string>
#endif
#ifndef LTMM_exception
# include <ltmm/exception.hh>
#endif

/** @file   backend.hh
    @author Christian Holm
    @date   Fri Jan 31 03:20:54 2003
    @brief  Base class for backend loaders */

namespace ltmm
{
  // Forward declaration 
  template <typename ThreadPolicy> class loader;
  template <typename ThreadPolicy> class backend;

  /** Friend to forward calls to open. 
      @param d The data - actual an object of this class. 
      @param f The file name of library to load
      @return A handle to the opened library, or 0 on error. */
  template <typename ThreadPolicy>
  lt_module 
  backend_open(lt_user_data d, const char* f) throw () 
  {
#ifdef HAVE_TEMPLATE_FRIEND
    return backend<ThreadPolicy>::open(d, f); 
#endif
  }
  /** Friend to forward calls to open.
      @param d The data - actual an object of this class. 
      @param m Handle to library to close
      @return 0 on success, 1 on errors. */
  template <typename ThreadPolicy>
  int 
  backend_close(lt_user_data d, lt_module m) throw() 
  {
#ifdef HAVE_TEMPLATE_FRIEND
    return backend<ThreadPolicy>::close(d, m); 
#endif
  }
  /** Friend to forward calls to find_symbol. 
      @param d The data - actual an object of this class. 
      @param m Handle to library to look in. 
      @param s Tjhe symbol to look for 
      @return pointer to symbol, or 0 (zero) on errors. */
  template <typename ThreadPolicy>
  lt_ptr 
  backend_find_sym(lt_user_data d, lt_module m, const char* s) throw() 
  {
#ifdef HAVE_TEMPLATE_FRIEND
    return backend<ThreadPolicy>::find_sym(d, m, s); 
#endif
  }
  /** Friend to forward calls to exit. */
  template <typename ThreadPolicy>
  int 
  backend_exit(lt_user_data d) throw ()
  {
#ifdef HAVE_TEMPLATE_FRIEND
    return backend<ThreadPolicy>::exit(d); 
#endif
  }

  /** @class backend ltmm/backend.hh <ltmm/backend.hh> 
      @brief Base class for backend loaders.  

      @par Error handling The user derived member method should throw
      an exception of kind ltmm::backend_error initialised with and
      appropiate error code identifier, if an error occours.  The
      library will process the exception and set the error condition
      as appropiate.
  */
  template <typename ThreadPolicy=single_thread>
  class backend
  {
  public:
    /// The type of thread locks 
    typedef ThreadPolicy thread_policy_type;    
  private:
  protected:
    /// Loader class needs to be a friend
    friend class loader<ThreadPolicy>;

    /// Name of this backend 
    std::string _name;
    /// Prefix for symbols 
    std::string _prefix;
    /// Low-level pointer 
    lt_dlloader* _place;

    /** Friend to forward calls to open. 
	@param d The data - actual an object of this class. 
	@param f The file name of library to load
	@return A handle to the opened library, or 0 on error. */
    static lt_module open(lt_user_data d, const char* f) throw ();
    /** Friend to forward calls to open.
	@param d The data - actual an object of this class. 
	@param m Handle to library to close
	@return 0 on success, 1 on errors. */
    static int close(lt_user_data d, lt_module m) throw();
    /** Friend to forward calls to find_symbol. 
	@param d The data - actual an object of this class. 
	@param m Handle to library to look in. 
	@param s Tjhe symbol to look for 
	@return pointer to symbol, or 0 (zero) on errors. */
    static lt_ptr find_sym(lt_user_data d, lt_module m,const char* s) throw();
    /** Friend to forward calls to exit. */
    static int exit(lt_user_data d) throw ();

#ifdef HAVE_TEMPLATE_FRIEND
    friend lt_module backend_open<ThreadPolicy>(lt_user_data d, const char* f) throw ();
    friend int backend_close<ThreadPolicy>(lt_user_data d, lt_module m) throw();
    friend lt_ptr backend_find_sym<ThreadPolicy>(lt_user_data d, lt_module m, const char* s) throw();
    friend int backend_exit<ThreadPolicy>(lt_user_data d) throw ();
#endif
    /** Constructor. */
    backend(const std::string& name, const std::string& prefix);
    /** Destructor. */
    virtual ~backend() {}

    /** Load a library.  The client should overload this to open a
	dynamic loadable library.  
	@param f The file name of the module to load. 
	@exception ltmm::backend See the class description. 
	@return a handle to the loaded library, or 0 on errors. */
    virtual void* load(const std::string& f) throw (backend_error) = 0;
    /** Unload a library. The client should overload this to close a
	dynamic loadable library.  
	@param m Handle to the library to unload. 
	@exception ltmm::backend See the class description. 
	@return true if successful, false otherwise  */
    virtual bool unload(void* m) throw (backend_error) = 0;
    /** Find a symbol in a library.  The client code should overload
	this member function to find a symbol in the loaded library. 
	@param m Handle to loaded library. 
	@param s The name of the symbol to find. 
	@exception ltmm::backend See the class description. 
	@return a handle to the symbol in the library, or 0 on
	errors. */ 
    virtual void* find_symbol(void* m, 
			      const std::string& s) throw (backend_error) = 0;
    /** Call-back called when the backend is removed. 
	The user cose should overload this to free any resources that
	the derived class may have. 
	@exception ltmm::backend See the class description. 
	@return false on errors, true on success. */
    virtual bool exit() throw (backend_error) = 0;    

  public:
    /** Get the name */
    const std::string& name() const { return _name; }
    /** Get the prefix */
    const std::string& prefix() const { return _prefix; }  
  };
  //__________________________________________________________________
  template <typename ThreadPolicy>
  backend<ThreadPolicy>::backend(const std::string& name, 
				 const std::string& prefix) 
    : _name(name), _prefix(prefix), _place(0) 
  {}


  //__________________________________________________________________
  template <typename ThreadPolicy>
  inline lt_module 
  backend<ThreadPolicy>::open(lt_user_data d, const char* f) throw ()
  {
    lt_module m = 0;
    try {
      backend<ThreadPolicy>* b = static_cast<backend<ThreadPolicy>*>(d);
      m = b->load(f);
      if (!m) throw backend_error(backend_error::cannot_open);
    }
    catch (backend_error& e) { 
      lt_dlseterror(e.id()); 
    }
    return m;
  }
  //__________________________________________________________________
  template <typename ThreadPolicy>
  inline int 
  backend<ThreadPolicy>::close(lt_user_data d, lt_module m) throw() 
  {
    int ret = 0;
    try {
      backend<ThreadPolicy>* b = static_cast<backend<ThreadPolicy>*>(d);
      if (b->unload(m)) throw backend_error(backend_error::cannot_close);
    }
    catch (backend_error& e) { 
      lt_dlseterror(e.id()); 
      ret = 1;
    }
    return ret;
  }
  //__________________________________________________________________
  template <typename ThreadPolicy>
  inline lt_ptr
  backend<ThreadPolicy>::find_sym(lt_user_data d, lt_module m,
				  const char* s) throw()
  {
    lt_ptr ss = 0;
    try {
      backend<ThreadPolicy>* b = static_cast<backend<ThreadPolicy>*>(d); 
      ss = (lt_ptr)b->find_symbol(m, s);
      if (!ss) throw backend_error(backend_error::symbol_not_found);
    }
    catch (backend_error& e) { 
      lt_dlseterror(e.id()); 
    }
    return ss;
  }
  //__________________________________________________________________
  template <typename ThreadPolicy>
  inline int 
  backend<ThreadPolicy>::exit(lt_user_data d) throw () 
  {
    int ret = 0;
    try {
      backend<ThreadPolicy>* b = static_cast<backend<ThreadPolicy>*>(d);
      if (b->exit()) throw backend_error(backend_error::remove_loader);
    }
    catch (backend_error& e) { 
      lt_dlseterror(e.id()); 
      ret = 1;
    }
    return ret;
  }

  //__________________________________________________________________
  /** @class internal_backend ltmm/backend.hh <ltmm/backend.hh> 
      @brief Base class for backend loaders.  
      This is used for the internal backends */
  template <typename ThreadPolicy> 
  class internal_backend : public backend<ThreadPolicy> 
  {
  protected:
    /// Loader class needs to be a friend
    friend class loader<ThreadPolicy>;

    /** Constructor. */
    internal_backend(lt_dlloader* p) 
      : backend<ThreadPolicy>(lt_dlloader_name(p), "") { backend<ThreadPolicy>::_place = p; }
    /** Dummy method. */
    void* load(const std::string&) throw(backend_error) { return 0; }
    /** Dummy method. */
    bool unload(void*) throw(backend_error) { return false; }
    /** Dummy method. */
    void* find_symbol(void*,const std::string&) throw(backend_error) { 
      return 0; }
    /** Dummy method. */
    bool exit() throw(backend_error) { return false; }
  };
}


#endif
//____________________________________________________________________
//
// EOF
//
