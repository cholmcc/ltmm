//
// $Id: symbol.hh,v 1.7 2005-04-07 13:28:24 cholm Exp $ 
//  
//  ltmm::symbol
//  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk> 
//
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public License 
//  as published by the Free Software Foundation; either version 2.1 
//  of the License, or (at your option) any later version. 
//
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free 
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
//  02111-1307 USA 
//
#ifndef LTMM_symbol
#define LTMM_symbol
#ifndef __LTDL__
#include <ltdl.h>
#endif
#ifndef __LIST__
#include <list>
#endif

/** @file   symbol.hh
    @author Christian Holm
    @date   Thu Jan 30 00:30:19 2003
    @brief  A class representing a symbol in the library */

namespace ltmm
{
  /** @class symbol ltmm/symbol.hh <ltmm/symbol.hh>
      @brief A class representing a symbol in the library. */
  class symbol
  {
  protected:
    /** The name of the symbol. */
    std::string _name;
    /** Pointer into library */
    lt_dlsymlist _data;

    /** Constructor. */
    symbol(lt_dlsymlist* ptr);
    /** Constructor. */
    symbol(const symbol& o);
    /** assignment */
    symbol& operator=(const symbol& o);
  public:
    /** Constructor. 
	@param ptr Pointer to function. 
	@param name Name of function. */
    symbol(void* ptr, const std::string& name);
    /** Destructor. */
    virtual ~symbol() {}
    /** Access the symbol pointer */
    void* ptr() { return (void*)_data.address; }    
    /** Get the name stored. */
    const std::string& name() const { return _name; }
  };
  //____________________________________________________________________
  inline 
  symbol::symbol(lt_dlsymlist* ptr)
  {
    if (!ptr) return;
    _name = ptr->name;
    _data.address = ptr->address; 
    _data.name    = _name.c_str();
  } 
  //____________________________________________________________________
  inline 
  symbol::symbol(void* ptr, const std::string& name) 
    : _name(name)
  {
    _data.address = (lt_ptr)ptr;
    _data.name    = _name.c_str();
  }
  //____________________________________________________________________
  inline 
  symbol::symbol(const symbol& o) 
    : _name(o._name)
  {
    _data.address = (lt_ptr)o._data.address;
    _data.name    = _name.c_str();
  }
  
  //____________________________________________________________________
  inline symbol& 
  symbol::operator=(const symbol& o) 
  { 
    _name         = o._name; 
    _data.address = o._data.address; 
    _data.name    = _name.c_str();
  }
  //____________________________________________________________________
  typedef std::list<symbol*> symbol_list;

  //====================================================================
  /** @struct variable ltmm/symbol.hh <ltmm/symbol.hh>
      @brief A global (exported) variable in a module 

      You can use this class as 
      @dontinclude super.cc 
      @skip variable
      @until cout
   */
  template <typename Type> 
  struct variable : public symbol
  {
    /** Type of the variable */
    typedef Type value_type;
    /** Reference type */
    typedef Type& reference_type;
    /** Create a variable symbol from a symbol. 
	@param s The symbol found in the module */
    variable(symbol* s) : symbol(*s) {}
    /** Access the symbol value as a simple variable */
    reference_type operator*()
    { 
      return *(static_cast<value_type*>(_data.address));
    }
  protected:
    /** Disallow copying */
    variable(const variable& v) {}
    /** Disallow Assignment */
    variable& operator=(const variable& v) { return *this; }
  };

  //====================================================================
  /** @struct function0 ltmm/symbol.hh <ltmm/symbol.hh>
      @brief A global (exported) function with no arguments in a module 

      You can use this class as 
      @dontinclude super.cc 
      @skip function0<void>
      @until foos()
      
      @note The future ISO/IEC C++ 0x may provide a @c std::functor
      class much like the one found in 
      <a href="http://sourceforge.net/projects/loki-lib/">loki</a> and
      <a href="http://www.boost.org/">Boost</a>.  When that happens,
      these should really be specialisation of that class template.
      For now, let this and function1 serve as an inspiration only. 

      @note This and the class template function1 could actually be
      united into one class, say @c function. function0 would then be
      a partial specialisation of the class template @c function: 
      @code 
         template <typename Return, 
	           typename Argument1, 
		   typename Argument2, 
		   ...> // Meaning `and so on' 
	 struct function; 

	 template <typename Return> 
	 struct function<Return, void, void, ...> 
	 { 
	   typedef Return return_type;
	   return_type operator()() { ... }
	   ...
	 };

	 template <typename Return, 
	           typename Argument1> 
	 struct function<Return, Argument1, void, ...> 
	 { 
	   typedef Argument1 argument1_type;
	   typedef Return    return_type;
	   return_type operator()(argument1_type a1) { ... }
	   ...
	 };
      @endcode 
      However, not all C++ compilers (MSVC 6) does not support partial
      template specialisations.   And, as this and function1 are
      really just examples, we go for portability. 

      @param Return the return value of the function 
   */
  template <typename Return>
  struct function0 : public symbol
  {
    /** Return value type */
    typedef Return return_type;
    /** Argument type */
    typedef void   argument_type;
    /** Function prototype type */
    typedef return_type (*function_type)(argument_type);
    /** Create a function symbol from a symbol. 
	@param s The symbol found in the module */
    function0(symbol* s) : symbol(*s) {}
    /** Evaluate the function 
	@return The return value of the function */
    return_type operator()() 
    {
      return return_type((*function_type(ptr()))());
    }
  protected:
    /** Disallow copying */
    function0(const function0& v) {}
    /** Disallow Assignment */
    function0& operator=(const function0& v) { return *this; }
  };

  template <>
  struct function0<void> : public symbol
  {
    /** Return value type */
    typedef void return_type;
    /** Argument type */
    typedef void   argument_type;
    /** Function prototype type */
    typedef return_type (*function_type)(argument_type);
    /** Create a function symbol from a symbol. 
	@param s The symbol found in the module */
    function0(symbol* s) : symbol(*s) {}
    /** Evaluate the function 
	@return The return value of the function */
    return_type operator()() 
    {
      (*function_type(ptr()))();
    }
  protected:
    /** Disallow copying */
    function0(const function0& v) : symbol(v) {}
    /** Disallow Assignment */
    function0& operator=(const function0& v) { return *this; }
  };

  //====================================================================
  /** @struct function1 ltmm/symbol.hh <ltmm/symbol.hh>
      @brief A global (exported) function in a module 

      You can use this class as 
      @dontinclude super.cc 
      @skip function1<int,int>
      @until s(20)
      
      @see function0
      @param Return the return value of the function 
      @param Arguments Argument type of the function 
   */
  template <typename Return, typename Arguments>
  struct function1 : public symbol
  {
    /** Return value type */
    typedef Return return_type;
    /** Argument type */
    typedef Arguments argument_type;
    /** Function prototype type */
    typedef return_type (*function_type)(argument_type);
    /** Create a function symbol from a symbol. 
	@param s The symbol found in the module */
    function1(symbol* s) : symbol(*s) {}
    /** Evaluate the function 
	@param a The function parameter 
	@return The return value of the function */
    return_type operator()(argument_type a) 
    {
      return return_type((*function_type(ptr()))(a));
    }
  protected:
    /** Disallow copying */
    function1(const function1& v) : symbol(v) {}
    /** Disallow Assignment */
    function1& operator=(const function1& v) { return *this; }
  };

}

#endif
//____________________________________________________________________
//
// EOF
//
