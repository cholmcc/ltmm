//
// $Id: handle.hh,v 1.6 2005-06-22 07:47:51 cholm Exp $ 
//  
//  ltmm::Handle
//  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk> 
//
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public License 
//  as published by the Free Software Foundation; either version 2.1 
//  of the License, or (at your option) any later version. 
//
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free 
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
//  02111-1307 USA 
//
#ifndef LTMM_handle
#define LTMM_handle
#ifndef __LTDL__
#include <ltdl.h>
#endif
#ifndef LTMM_single_thread
#include <ltmm/single_thread.hh>
#endif
#ifndef LTMM_exception
#include <ltmm/exception.hh>
#endif
#ifndef LTMM_symbol
#include <ltmm/symbol.hh>
#endif

namespace ltmm
{
  // Forward declaration 
  template <typename ThreadPolicy> class loader;


  //====================================================================
  /** @class handle ltmm/handle.hh <ltmm/handle.hh> 
      @brief Handle on dynamic libraries. 
      Note, that this class is not constructable, destructable, or
      assignable.  The only way to make objects of this class, is to
      get them as references from the loader, and the only way to free
      them is to close the handles via the loader.  The libtool C
      library provides a mechanism for associating user data with the
      handles.  That is not possible directly via this class.
      Instead, the application should sub-class this class to contain
      any kind of user data it may need. All data members are
      protected, and all member functions are virtual, so it should be
      quite flexible. */
  template <typename ThreadPolicy=single_thread>
  class handle 
  {
  public:
    /// Type of mutually exclusive locks
    typedef ThreadPolicy thread_policy_type;
  protected:
    /// Loader class needs to be a friend
    friend class loader<thread_policy_type>;

    /// The actual handle to the library. 
    lt_dlhandle _handle;
    /// Information on the handle 
    mutable const lt_dlinfo* _info;

    /** Get the information on the library. 
	@exception ltmm::exception thrown in case of error. */
    virtual void info() const;

    /** Protect the constructor. 
	@param h Low-level handle to library */
    handle(lt_dlhandle h) : _handle(h), _info(0) {}
    /** Protect the copy constructor. */
    handle(const handle& h) : _handle(h._handle), _info(h._info) {}
    /** Protect the destructor. */
    virtual ~handle() {}
    /** Protect the assignment operator. */
    virtual handle& operator=(const handle& h) { return *this; }
  public:
    /** Protect this library from being unloaded.  After this message
	has been sent, the library can not be unloaded by
	loader::unload.  Note, that the destructor will still
	invalidate this handle. 
	@exception ltmm::exception thrown in case of error. */
    virtual void make_resident() const;

    /** Test if this library is resident.  Check if this library can
	be unloaded. Note, that a true return value does not mean that
	the library will be unloaded on the next loader::unload - only
	that it @e can can be unloaded.  
	@exception ltmm::exception thrown in case of error.
	@return true if resident, false otherwise. */
    virtual bool is_resident() const;

    /** The reference count.  Return how many times this lobrary has
	been loaded.
	@exception ltmm::exception thrown in case of error.
	@return the reference count. */
    virtual int reference_count() const;

    /** The filename of the library file. 
	@exception ltmm::exception thrown in case of error. 
	@return the filename of the library file */
    virtual std::string file_name() const;

    /** Get the module name.  If this is a module, then this will
	return the module name, or the empty string.
	@exception ltmm::exception thrown in case of error. 
	@return the module name. */
    virtual std::string module_name() const;

    /** Try to find a symbol in the library.  This will lookup the raw
	symbol in the library, and return a handle to it, if it
	exists.  Note that the symbol is unique and should be
	deallocated by the caller.  No caching, apart from libltdl's
	internal caching, is done.
	@exception ltmm::exception thrown in case of error. 
	@param symbol The symbol name to lookup. 
	@return Handle to the symbol. */
    virtual symbol* find_symbol(const std::string& symbol) const;
  };
  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline void handle<ThreadPolicy>::make_resident() const
  {
    if (lt_dlmakeresident(_handle)) throw ltmm::exception();
  }

  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline bool handle<ThreadPolicy>::is_resident() const
  {
    int ret = lt_dlisresident(_handle);
    if (ret < 0) throw ltmm::exception();
    return ret == 1 ? true : false;
  }

  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline void handle<ThreadPolicy>::info() const
  {
    if (!_info) _info = lt_dlgetinfo(_handle);
    if (!_info) throw ltmm::exception(); 
  }

  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline int handle<ThreadPolicy>::reference_count() const
  {
    info();
    return _info->ref_count;
  }

  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline std::string handle<ThreadPolicy>::file_name() const
  {
    info();
    return std::string(_info->filename);
  }

  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline std::string handle<ThreadPolicy>::module_name() const
  {
    info();
    return std::string(_info->name);
  }

  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline symbol* 
  handle<ThreadPolicy>::find_symbol(const std::string& name) const
  {
    lt_ptr symb = lt_dlsym(_handle, name.c_str());
    if (!symb) throw ltmm::exception();
    return new symbol(symb, name);
  }
}

#endif
//____________________________________________________________________
//
// EOF
//
