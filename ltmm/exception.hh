//
// $Id: exception.hh,v 1.5 2004-10-31 12:12:08 cholm Exp $ 
//  
//  ltmm::exception
//  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk> 
//
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public License 
//  as published by the Free Software Foundation; either version 2.1 
//  of the License, or (at your option) any later version. 
//
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free 
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
//  02111-1307 USA 
//
#ifndef LTMM_exception
#define LTMM_exception
#ifndef LTDL_H
#include <ltdl.h>
#endif
#ifndef __STRING__
#include <string>
#endif
#ifndef __STDEXCEPT__
#include <stdexcept>
#endif

/** @file   exception.hh
    @author Christian Holm
    @date   Wed Jan 29 19:32:39 2003
    @brief  Exception class for handling errors from the libltdl
    library and user defined loaders.  */ 

namespace ltmm
{
  /** @class exception ltmm/exception.hh <ltmm/exception.hh> 
      @brief Exception class for handling errors from the libltdl
      library and user defined loaders. 
   */
  class exception : public std::exception
  {
  protected:
    /// The message 
    std::string _what;
  public:
    /** Constructor. It retrives the error message from the libltdl
	library */
    exception() : _what(lt_dlerror()) {}
    /** Constructor. Sets the error message from argument. */
    exception(const std::string& s) : _what(s) {}
    /** Destructor. */
    virtual ~exception() throw() {}
    /** Get the error message. */
    virtual const char* what() const throw() { return _what.c_str(); }
  };

  /** @class backend_error ltmm/exception.hh <ltmm/exception.hh> 
      @brief Exception class for backend loaders. */ 
  class backend_error : public std::exception 
  {
  protected:
    /// The identifier 
    int _id;
  public:
    /// The error codes defined. 
    enum {                  /// unknown error			    
      unknown,              /// dlopen support not available	     
      dlopen_not_supported, /// invalid loader			     
      invalid_loader,       /// loader initialization failed	     
      init_loader,          /// loader removal failed		     
      remove_loader,        /// file not found			     
      file_not_found,       /// dependency library not found	     
      deplib_not_found,     /// no symbols defined		     
      no_symbols,           /// can't open the module		     
      cannot_open,          /// can't close the module		     
      cannot_close,         /// symbol not found		     
      symbol_not_found,     /// not enough memory		     
      no_memory,            /// invalid module handle		     
      invalid_handle,       /// internal buffer overflow	     
      buffer_overflow,      /// invalid errorcode		     
      invalid_errorcode,    /// library already shutdown	     
      shutdown,             /// can't close resident module	     
      close_resident_module,/// invalid mutex handler registration  
      invalid_mutex_args,   /// invalid search path insert position 
      invalid_position       
    };
    /** Constructor. 
	@param id the error code. */
    backend_error(int id) : _id(id) {}
    /** Constructor. 
	This actually registers a new error with the libltdl library. 
	@param s the error string. */
    backend_error(const std::string& s) : _id(0) 
    {
      _id = lt_dladderror(s.c_str());
      if (_id < 0) _id = invalid_errorcode;
    }
    /** Get the id */
    int id() const { return _id; }
  };
}

#endif
//____________________________________________________________________
//
// EOF
//
