//
// $Id: loader.hh,v 1.16 2005-04-07 13:28:24 cholm Exp $ 
//  
//  ltmm::Loader
//  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk> 
//
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public License 
//  as published by the Free Software Foundation; either version 2.1 
//  of the License, or (at your option) any later version. 
//
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free 
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
//  02111-1307 USA 
//
#ifndef LTMM_loader
#define LTMM_loader
#ifndef LTMM_exception
#include <ltmm/exception.hh>
#endif
#ifndef LTMM_single_thread
#include <ltmm/single_thread.hh>
#endif
#ifndef LTMM_handle
#include <ltmm/handle.hh>
#endif
#ifndef LTMM_backend
#include <ltmm/backend.hh>
#endif
#ifndef LTDL_H
#include <ltdl.h>
#endif
#ifdef __GNUC__
# if __GNUC__ >= 3
/** @brief the `preloaded' symbols.  
    This is only referenced if ltmm::loader<>::preloaded() is seen by
    the linker. */
extern const ::lt_dlsymlist lt_preloaded_symbols[]; 
# endif
#elif defined(__WINDOWS__) && defined(_MSC_VER)
extern "C" { extern const ::lt_dlsymlist lt_preloaded_symbols[]; }
#else 
extern const ::lt_dlsymlist lt_preloaded_symbols[]; 
#endif	
#ifndef __UTILITY__
# include <utility>
#endif
#ifndef __MAP__
# include <map>
#endif

namespace ltmm
{
  // Forward decl. 
  template <typename ThreadPolicy> 
  class loader;
  
  /** Friend function to do locking */
  template <typename ThreadPolicy> 
  void 
  loader_lock() 
  { 
    loader<ThreadPolicy>::_lock.lock(); 
  }
  /** Friend function to do locking */
  template <typename ThreadPolicy> 
  void 
  loader_unlock() 
  { 
    loader<ThreadPolicy>::_lock.unlock(); 
  }
  /** Friend function to do locking */
  template <typename ThreadPolicy> 
  void 
  loader_set_error(const char* m) 
  { 
    loader<ThreadPolicy>::_lock.set_error(m); 
  }
  /** Friend function to do locking */
  template <typename ThreadPolicy> 
  const char* loader_get_error() 
  { 
    return loader<ThreadPolicy>::_lock.get_error(); 
  }
  
  /** @class loader ltmm/loader.hh <ltmm/loader.hh>
      @brief Class to load dynamic libraries. 
      
      @par Loading libraries - ltmm::loader<ThreadPolicy>::load

      If @p filename is the empty string and the program was linked
      with @c -export-dynamic or @c -dlopen self, this member function
      will return a handle for the program itself, which can be used
      to access its symbols.

      If the member function cannot find the library and the file name
      @p filename does not have a directory component it will
      additionally search in the following search paths for the module
      (in the order as follows):

      <dl>
        <dt> user-defined search path</dt> 
        <dd> This search path can be set by the program using the
          member functions ltmm::loader<ThreadPolicy>::search_path and 
          ltmm::loader<ThreadPolicy>::addto_search_path.</dd>
        <dt> library's search path </dt>
        <dd> This search path is the value of the environment variable
          @c LTDL_LIBRARY_PATH. </dd>
        <dt> system library search path </dt> 
        <dd> The system dependent library search path (e.g. on 
          @b GNU/Linux it is @c LD_LIBRARY_PATH) </dd>
      </dl>

      Each search path must be a colon-separated list of absolute
      directories, for example, @c "/usr/lib/mypkg:/lib/foo".

      If the file with the file name @p filename cannot be found
      the member function tries to append the following extensions: 

      <ol>
	<li> the libtool archive extension @c .la </li>
	<li> the extension used for native dynamic libraries on the
	host platform, e.g., @c .so, @c .sl, etc.</li>
      </ol>

      @param ThreadPolicy A policy object that says how to lock
      structures in a multi-threaded environment.  Per default it is
      ltmm::single_thread, but see alse that class for more
      documentation. 
  */
  template <typename ThreadPolicy=single_thread>
  class loader
  {
  public:
    /// The type of thread locks 
    typedef ThreadPolicy thread_policy_type;
    /// Type of handles 
    typedef handle<thread_policy_type> handle_type;
    /// Type of backends
    typedef backend<thread_policy_type> backend_type;
    /// The type of the map of symbol lists. 
    typedef std::map<std::string, symbol_list> symbol_map;
  protected:
    /** @class guard ltmm/loader.hh <ltmm/loader.hh> 
	@brief Guard class */
    struct guard 
    {
      /// Reference to lock
      thread_policy_type& _lock;
      /// Construct the guard, acquiring the lock
      guard(thread_policy_type& l) : _lock(l) { _lock.lock(); }
      /// Destruct the guard, releasing the lock
      ~guard() { _lock.unlock(); }
    };
    /// Static (singleton) instance 
    static loader* _instance;
    /// Static lock 
    static thread_policy_type _lock;
    /// Map of preloaded symbols.  
    static symbol_map _preloaded;

    /// Caller ID to resolve user data in modules. 
    lt_dlcaller_id _id;
    /** Constructor.
	The constructor is protected, so that the client code can only
	access the loader via the singleton interface.
	@exception ltmm::exception is thrown if the library could not
	be initialised. */
    loader();
    /** Destructor. 
	The destructor is protected, so that the client code can only
	access the loader via the singleton interface. */
    virtual ~loader();

#ifdef HAVE_TEMPLATE_FRIEND
    //@{
    /// @name Thread-safe locking
    /** Friend function to do locking */
    friend void loader_lock<ThreadPolicy>();
    /** Friend function to do locking */
    friend void loader_unlock<ThreadPolicy>();
    /** Friend function to do locking */
    friend void loader_set_error<ThreadPolicy>(const char* m);
    /** Friend function to do locking */
    friend const char* loader_get_error<ThreadPolicy>();
    //@}

    /// Handle iterator is a friend 
    friend class handle_iterator;
    /// Backend iterator is a friend 
    friend class backend_iterator;
#else
  public:
#endif
    /** Static member function used by handle_iterator */
    handle_type* next_handle(handle_type* h);
    /** Static member function used by backend_iterator */
    backend_type* next_backend(backend_type* h);
  public:
    /** Get the static (singleton) object. */
    static loader& instance(); 

    /// @name Loading and unloading libraries
    //@{
    /** Load a dynamic library. 
	If the same module is loaded several times, the same handle is 
	returned.  See also class description.
	@param filename Is the file name of the library to load. 
	@return A handle to the library loaded. 
	@exception ltmm::exception If the method fails, an exception
	is thrown. 
    */
    handle_type& load(const std::string& filename);
    /** Unload a library. 
	Decrement the reference count on the module @p handle.  If it 
	drops to zero and no other module depends on this module, then
	the module is unloaded. 
	@exception ltmm::exception If the method fails.
	@param h The library to unload. */
    void unload(handle_type& h);
    //@}

    /// @name The user-defined search path
    //@{
    /** Add a directory to user-defined library search path.
	@param dir Directory to add. 
	@exception ltmm::exception on errors. */
    void addto_search_path(const std::string& dir);
    /** Set the user-defined library search path. 
	@param path the new user-defined search path. 
	@return the search path. 
	@exception ltmm::exception on errors. */
    std::string search_path(const std::string& path);
    /** Get the user-defined library search path. 
	@return the search path. 
	@exception ltmm::exception on errors. */
    std::string search_path() const;
    //@}

    /** @class handle_iterator ltmm/loader.hh <ltmm/loader.hh> 
	@brief Forward iterator over handles in list. */
    class handle_iterator 
    {
    public:
      /// Type of loeader
      typedef loader<thread_policy_type> loader_type;
      /// Need to be friend to call protected ctor
      friend class loader<thread_policy_type>;
    protected:
      /** Pointer to loader */
      loader_type* _loader;
      /** The current handle */
      handle_type* _current;
      /** Check that the current handle is valid 
	  @exception ltmm::exception if the handle is invalid 
	  @return Pointer to current handle  */
      handle_type* check_current() const
      {
	if (!_current) throw ltmm::exception("handle_iterator out of range");
	return _current;
      }
      /** Protect constructor 
	  @param l Loader to make iterator over. 
	  @param h The `current' handle to start at. */
      handle_iterator(loader<ThreadPolicy>* l, handle_type* h)
	: _loader(l), _current(h) 
      {}
    public:
      /** Destructor */
      ~handle_iterator() {}
      /** Access the handle, if any.
	  @exception ltmm::exception if deferencing after end.
	  @return reference to handle. */
      handle_type& operator*() { return *check_current(); }
      /** Increment the iterator. */
      handle_iterator operator++(int i) 
      {
	handle_iterator tmp = *this;
	_current = _loader->next_handle(check_current());
	return tmp;
      }
      /** Increment the iterator. */
      handle_iterator& operator++()
      {
	_current = _loader->next_handle(check_current());
	return *this;
      }
      /** Compare to other iterator. */
      bool operator==(const handle_iterator& o) const 
      {
	return (o._current == _current);
      }
      /** Compare to other iterator. */
      bool operator!=(const handle_iterator& o) const 
      {
	return !(this->operator==(o));
      }
      /** Compare to other iterator. */
      bool operator<(const handle_iterator& o) const 
      {
	return (_current < o._current);
      }
      /** Assign from another iterator */
      handle_iterator& operator=(const handle_iterator& o) 
      {
	_loader = o._loader; _current = o._current; return *this;
      }
    };

    /// @name Iteration on handles
    //@{
#ifdef HAVE_TEMPLATE_MEMFUNC
    /** Iterate over all loaded modules. 
	@param f Is an object of type @p Functor. It must define the
	member function @c int @c operator()(ltmm::handle&). If that
	member function at any given point returns a non-zero value,
	the loop is stopped at that iteration. 
	@return true if all handles was visited, or false if
	interrupted. */
    template<typename Functor> bool foreach_handle(Functor& f);
#endif

    /** Return an iterator pointing at start of handle list. */
    handle_iterator begin_handle();
    /** return an iterator pointing just after end of list. */
    handle_iterator end_handle();
    //@}

    /** @class backend_iterator ltmm/loader.hh <ltmm/loader.hh> 
	@brief Forward iterator over backends in list. */
    class backend_iterator 
    {
    public:
      /// Type of loeader
      typedef loader<thread_policy_type> loader_type;
      /// Need to be friend to call protected ctor
      friend class loader<thread_policy_type>;
    protected:
      /** Pointer to loader */
      loader_type* _loader;
      /** The current backend */
      backend_type* _current;
      /** Check if the current backend is valid.  If not, throw an
	  exception.  If it is valid, then return it. 
	  @exception ltmm::exception 
	  @return  Pointer to the current backend*/
      backend_type* check_current() const 
      {
	if (!_current) throw ltmm::exception("backend_iterator out of range");
	return _current;
      }
      /** Protect constructor 
	  @param l Pointer to loader singleton 
	  @param c The `current' backend, to start the iteration
	  from. */
      backend_iterator(loader<ThreadPolicy>* l, backend_type* c) 
	: _loader(l), _current(c) 
      {}      
    public:
      /** Proctect destructor */
      ~backend_iterator() {}
      /** Access the backend, if any.
	  @exception ltmm::exception if deferencing after end.
	  @return reference to backend. */
      backend_type& operator*() { return *check_current(); }
      /** Increment the iterator. */
      backend_iterator operator++(int i)
      {
	backend_iterator tmp = *this;
	_current = _loader->next_backend(check_current());
	return tmp;
      }
      /** Increment the iterator. */
      backend_iterator& operator++()
      {
	_current = _loader->next_backend(check_current());
	return *this;
      }
      /** Compare to other iterator. */
      bool operator==(const backend_iterator& o) const 
      {
	return _current == o._current;
      }
      /** Compare to other iterator. */
      bool operator!=(const backend_iterator& o) const 
      {
	return !(this->operator==(o));
      }
      /** Compare to other iterator. */
      bool operator<(const backend_iterator& o) const 
      {
	return (_current < o._current);
      }
      /** Assign from another iterator */
      backend_iterator& operator=(const backend_iterator& o) 
      {
	_loader = o._loader; _current = o._current; return *this;
      }
    };

    /// @name Backend handling and iteration
    //@{
    /** Add a backend to the list of loaders. 
	@param b The loader to add. */
    void insert_backend(backend_type& b);
    /** Insert @e after an iterator position. */
    void insert_backend(const backend_iterator& i, backend_type& b);
    /** Remove a backend from the list. */
    void remove_backend(backend_iterator& b);
    /** Remove a backend from the list. */
    void remove_backend(const std::string& name);
    /** Find a backend loader. */
    backend_iterator find_backend(const std::string& name); 
    /** Get a forward iterator pointing to the first element in the
	backend list. */ 
    backend_iterator begin_backend();
    /** Get a forward iterator pointing to just afte the end of the
	backend list. */  
    backend_iterator end_backend();
    //@}

    
    /// @name Preloading
    //@{
    /** Get (and set internally) list of preloaded symbols.  
	Note, that it is important that the program is linked with the
	@c -dlopen option passed to @b Libtool for this to work.  The
	reason is, that it references an external symbol that is only
	defined if @c -dlopen is used.  If the application isn't
	linked with @c -dlopen, then it will result in a compile time
	error of an undefined reference to @c lt_preloaded_symbols. *
	@exception ltmm::exception Thrown in case of errors. 
	@return map of preloaded symbols.  The map consist of a key,
	which is the file where the symbols is founds, and a value of
	a ltmm::symbol_list, which is a list of ltmm::symbol
	objects. The key @c \@PROGRAM\@ denotes the application
	itself. Note, that the names are mangled, so they may look  
	very odd indeed.  */
    static symbol_map& preloaded();
    /** Add a symbol to the preloaded symbols. 
	@exception ltmm::exception Thrown in case of errors. 
	@param s The symbol to add. */
    void preload(symbol& s);
    //@}
  };
  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline loader<ThreadPolicy>::loader()
  {
    // Initialise the library 
    int nErrors = lt_dlinit();
    if (nErrors != 0) throw ltmm::exception();
    // Get the caller identifier from library. 
    if (!_id) _id = lt_dlcaller_register();
    // Register the threading model 
#ifdef HAVE_TEMPLATE_FRIEND
    if (lt_dlmutex_register(&loader_lock<ThreadPolicy>,
			    &loader_unlock<ThreadPolicy>,
			    &loader_set_error<ThreadPolicy>,
			    &loader_get_error<ThreadPolicy>)) 
      throw ltmm::exception();
#endif
  }

  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline loader<ThreadPolicy>::~loader()
  {
    int nErrors = lt_dlexit();
    if (nErrors != 0) throw ltmm::exception();
    if (_instance == this) _instance = 0;
  }

  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline loader<ThreadPolicy>& loader<ThreadPolicy>::instance() 
  {
    // Use double-checking idom
    if (!_instance) {
      guard g(_lock);
      if (!_instance)
	_instance = new loader<ThreadPolicy>();
    }
    return *_instance;
  }

  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline handle<ThreadPolicy>& 
  loader<ThreadPolicy>::load(const std::string& filename) 
  {
    guard g(_lock);
    lt_dlhandle lthandle;
    if (filename.empty()) lthandle = lt_dlopenext(0);
    else                  lthandle = lt_dlopenext(filename.c_str());
    if (!lthandle) throw ltmm::exception();
 
    handle_type* h = 0;;
    // The hack with the reference count is ugly, but libltdl does
    // not allow us to get user data that hasn't previously been
    // set! Really stupid, espcially as it casues a SIGSEGV.
    const lt_dlinfo* info = lt_dlgetinfo(lthandle);
    if (info->ref_count == 1 || 
	!(h = (handle_type*)lt_dlcaller_get_data(_id, lthandle))) {
      h = new handle_type(lthandle);
      lt_ptr old = lt_dlcaller_set_data(_id, lthandle, (void*)h);
      // It says in the manual that this is how we should do the
      // checks, but that's wrong - lt_dlerror never ever returns an
      // empty string!  What the h**l are they up to?
      // if (!old) {
      //   const char* msg = lt_dlerror ();
      //   if (msg) throw ltmm::exception(msg);
      // } 
    }
    return *h;
  }

  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline void loader<ThreadPolicy>::unload(handle_type& h) 
  {
    guard g(_lock);
    int ref = h.reference_count();
    int ret = lt_dlclose(h._handle);
    if (ret) throw ltmm::exception();
    if (ref == 1) { 
      lt_ptr old = lt_dlcaller_set_data(_id, h._handle, 0);
      delete &h;
    }
  }

  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline void loader<ThreadPolicy>::addto_search_path(const std::string& dir) 
  {
    guard g(_lock);
    if (lt_dladdsearchdir(dir.c_str())) throw ltmm::exception();
    _lock.unlock();    
  }

  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline std::string loader<ThreadPolicy>::search_path(const std::string& path) 
  {
    guard g(_lock);
    if (lt_dlsetsearchpath(path.c_str())) throw ltmm::exception();
    if (!lt_dlgetsearchpath()) return std::string();
    return std::string(lt_dlgetsearchpath());
  }

  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline std::string loader<ThreadPolicy>::search_path() const
  {
    guard g(_lock);
    if (!lt_dlgetsearchpath()) return std::string();
    return std::string(lt_dlgetsearchpath());
  }

#if HAVE_TEMPLATE_MEMFUNC
  //____________________________________________________________________
  template <typename ThreadPolicy>
  template <typename Functor>
  inline bool loader<ThreadPolicy>::foreach_handle(Functor& f) 
  {
    std::pair<lt_dlcaller_id, Functor*> d(_id,&f);
    struct trampoline 
    {
      static int function(lt_dlhandle h, void* d) {
	std::pair<lt_dlcaller_id, Functor*>* p =
	  static_cast<std::pair<lt_dlcaller_id, Functor*>*>(d);
	if (!d) return 1;
	handle_type* hh = 
	  static_cast<handle_type*>(lt_dlcaller_get_data(p->first, h));
	return p->second->operator()(*hh);
      }
    };
    int ret = lt_dlforeach(trampoline::function, &d);
    return ret == 0 ? true : false;
  }
#endif

  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline handle<ThreadPolicy>*
  loader<ThreadPolicy>::next_handle(handle_type* h)
  {
    lt_dlhandle lh = lt_dlhandle_next(!h ? 0 : h->_handle);
    if (!lh) return 0;
    handle_type* hh = static_cast<handle_type*>(lt_dlcaller_get_data(_id, lh));
    return hh;
  }

#if 0
  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline 
  loader<ThreadPolicy>::handle_iterator::handle_iterator(loader<ThreadPolicy>*l, 
							 handle_type* h)
    : _loader(l), _current(h) 
  {}

  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline handle<ThreadPolicy>&
  loader<ThreadPolicy>::handle_iterator::operator*() 
  {
    if (!_current) throw ltmm::exception("handle_iterator out of range");
    return *_current;
  }
  
  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline 
  typename loader<ThreadPolicy>::handle_iterator&
  loader<ThreadPolicy>::handle_iterator::operator++() 
  {
    if (!_current) throw ltmm::exception("handle_iterator out of range");
    _current = _loader->next_handle(_current);
    return *this;
  }

  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline 
  typename loader<ThreadPolicy>::handle_iterator
  loader<ThreadPolicy>::handle_iterator::operator++(int) 
  {
    handle_iterator tmp = *this;
    if (!_current) throw ltmm::exception("handle_iterator out of range");
    _current = _loader->next_handle(_current);
    return tmp;
  }
  
  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline 
  typename loader<ThreadPolicy>::handle_iterator&
  loader<ThreadPolicy>::handle_iterator::operator=(const handle_iterator& o) 
  {
    _current = o._current;
    _loader  = o._loader;
    return *this;
  }
  
  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline 
  bool
  loader<ThreadPolicy>::handle_iterator::operator==(const handle_iterator& o) 
    const
  {
    return (o._current == _current);
  }

  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline 
  bool
  loader<ThreadPolicy>::handle_iterator::operator!=(const handle_iterator& o) 
    const
  {
    return !(o == *this);
  }
  
  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline 
  bool
  loader<ThreadPolicy>::handle_iterator::operator<(const handle_iterator& o) 
    const
  {
    return (o._current < _current);
  }
#endif
  
  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline 
  typename loader<ThreadPolicy>::handle_iterator 
  loader<ThreadPolicy>::begin_handle()
  {
    handle_type* hh = next_handle(0);
    handle_iterator i(this, hh);
    return i;
  }
  
  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline 
  typename loader<ThreadPolicy>::handle_iterator 
  loader<ThreadPolicy>::end_handle()
  {
    handle_iterator i(this, 0);
    return i;
  }

  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline 
  backend<ThreadPolicy>*
  loader<ThreadPolicy>::next_backend(backend_type* b)
  {
    lt_dlloader* p  = 0;
    if (b && b->_place) p = b->_place;
    lt_dlloader* ll = lt_dlloader_next(p);
    if (!ll) return 0;
    lt_user_data* dd = lt_dlloader_data(ll);
    backend_type* bb = 0;
    if (!*dd) {
      // A system backend - set the data 
      bb = new internal_backend<ThreadPolicy>(ll);
      *dd = (void*)bb;
    }
    else 
      bb = static_cast<backend_type*>(*dd);
    return bb;
  }

#if 0
  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline 
  loader<ThreadPolicy>::backend_iterator::backend_iterator(loader<ThreadPolicy>*l, 
							   backend_type* b)
    : _loader(l), _current(b) 
  {}

  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline 
  backend<ThreadPolicy>&
  loader<ThreadPolicy>::backend_iterator::operator*() 
  {
    if (!_current) throw ltmm::exception("backend_iterator out of range");
    return *_current;
  }
  
  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline 
  typename loader<ThreadPolicy>::backend_iterator&
  loader<ThreadPolicy>::backend_iterator::operator++() 
  {
    if (!_current) throw ltmm::exception("backend_iterator out of range");
    _current = _loader->next_backend(_current);
    return *this;
  }

  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline 
  typename loader<ThreadPolicy>::backend_iterator
  loader<ThreadPolicy>::backend_iterator::operator++(int) 
  {
    backend_iterator tmp = *this;
    if (!_current) throw ltmm::exception("backend_iterator out of range");
    _current = _loader->next_backend(_current);
    return tmp;
  }

  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline 
  typename loader<ThreadPolicy>::backend_iterator&
  loader<ThreadPolicy>::backend_iterator::operator=(const backend_iterator& o) 
  {
    _current = o._current;
    _loader  = o._loader;
    return *this;
  }
  
  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline 
  bool
  loader<ThreadPolicy>::backend_iterator::operator==(const backend_iterator& o)
    const
  {
    return (o._current == _current);
  }
  
  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline 
  bool
  loader<ThreadPolicy>::backend_iterator::operator!=(const backend_iterator& o)
    const
  {
    return ! (o == *this);
  }

  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline 
  bool
  loader<ThreadPolicy>::backend_iterator::operator<(const backend_iterator& o) 
    const
  {
    return (o._current < _current);
  }
#endif 
  
  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline 
  typename loader<ThreadPolicy>::backend_iterator 
  loader<ThreadPolicy>::begin_backend()
  {
    backend_type* hh = next_backend(0);
    backend_iterator i(this, hh);
    return i;
  }
  
  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline 
  typename loader<ThreadPolicy>::backend_iterator 
  loader<ThreadPolicy>::end_backend()
  {
    backend_iterator i(this, 0);
    return i;
  }

  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline 
  void 
  loader<ThreadPolicy>::insert_backend(backend_type& b) 
  {
    insert_backend(end_backend(), b);
  }

  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline 
  void 
  loader<ThreadPolicy>::insert_backend(const backend_iterator& i, 
				       backend_type& b) 
  {
    if (b.name().empty()) return;
    struct lt_user_dlloader l;
    l.sym_prefix        = b.prefix().c_str();
    l.module_open       = backend_open<ThreadPolicy>;
    l.module_close      = backend_close<ThreadPolicy>;
    l.find_sym          = backend_find_sym<ThreadPolicy>;
    l.dlloader_exit     = backend_exit<ThreadPolicy>;
    l.dlloader_data     = static_cast<lt_user_data>(&b);
    int ret = lt_dlloader_add(i == end_backend() || !i._current
			      ? 0 : i._current->_place,
			      &l, b.name().c_str());
    if (ret != 0) throw exception();
    lt_dlloader* p = lt_dlloader_find(b.name().c_str());
    if (!p)  throw exception();
    b._place = p;
  }

  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline 
  void 
  loader<ThreadPolicy>::remove_backend(backend_iterator& i) 
  {
    if (i == end_backend() || !i._current) return;
    remove_backend(i._current->name());
  }

  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline 
  void 
  loader<ThreadPolicy>::remove_backend(const std::string& name) 
  {
    if (name.empty()) return;
    if (lt_dlloader_remove(name.c_str() != 0))
      throw exception();
  }

  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline 
  typename loader<ThreadPolicy>::backend_iterator 
  loader<ThreadPolicy>::find_backend(const std::string& name) 
  {
    backend_type* b = 0;
    lt_dlloader* l = lt_dlloader_find(name.c_str());
    if (!l) return end_backend();
    lt_user_data* d = lt_dlloader_data(l);
    if (!*d) {
      // A system backend - set the data 
      b = new internal_backend<ThreadPolicy>(l);
      *d = (void*)b;
    }
    else 
      b = static_cast<backend_type*>(*d);
    return backend_iterator(this, b);
  }

  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline 
  typename loader<ThreadPolicy>::symbol_map&
  loader<ThreadPolicy>::preloaded() 
  {
    if (_preloaded.size() == 0) {
      // Make sure we're exclusive 
      guard g(_lock);
      if (_preloaded.size() == 0) {
	const lt_dlsymlist* now;
#if defined(__GNUC__) && __GNUC__ < 3
	extern const ::lt_dlsymlist lt_preloaded_symbols[]; 
#endif	
	if (lt_dlpreload_default(lt_preloaded_symbols) != 0) 
	  throw ltmm::exception();
	now = lt_preloaded_symbols;
	std::string fn;
	while (now->name != 0) {
	  if (!now->address) 
	    // Found a file 
	    fn = now->name;
	  else 
	    // Found a symbol 
	    _preloaded[fn].push_back(new symbol(now->address, now->name));
	  now++;
	}
      }
    }
    return _preloaded;
  }
  //____________________________________________________________________
  template <typename ThreadPolicy>
  inline 
  void
  loader<ThreadPolicy>::preload(symbol& s) 
  {
    lt_dlsymlist sl;
    sl.name = s.name().c_str();
    sl.address = s.ptr();
    if (lt_dlpreload(&sl) != 0) 
      throw ltmm::exception();
  }
  
  //__________________________________________________________________
  template <typename ThreadPolicy> 
  loader<ThreadPolicy>* loader<ThreadPolicy>::_instance = 0;

  //__________________________________________________________________
  template <typename ThreadPolicy> 
   ThreadPolicy loader<ThreadPolicy>::_lock;
  //__________________________________________________________________
  template <typename ThreadPolicy> 
  std::map<std::string,symbol_list> loader<ThreadPolicy>::_preloaded;
}

#endif
//____________________________________________________________________
//
// EOF
//
