//
// $Id: single_thread.hh,v 1.5 2005-01-12 18:17:48 cholm Exp $ 
//  
//  ltmm::single_thread
//  Copyright (C) 2002 Christian Holm Christensen <cholm@nbi.dk> 
//
//  This library is free software; you can redistribute it and/or 
//  modify it under the terms of the GNU Lesser General Public License 
//  as published by the Free Software Foundation; either version 2.1 
//  of the License, or (at your option) any later version. 
//
//  This library is distributed in the hope that it will be useful, 
//  but WITHOUT ANY WARRANTY; without even the implied warranty of 
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
//  Lesser General Public License for more details. 
// 
//  You should have received a copy of the GNU Lesser General Public 
//  License along with this library; if not, write to the Free 
//  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 
//  02111-1307 USA 
//
#ifndef LTMM_single_thread
#define LTMM_single_thread

/** @file   single_thread.hh
    @author Christian Holm
    @date   Wed Jan 29 21:21:02 2003
    @brief  Utility class. This class is used to lock to insure proper
      allocation of the singletons in the library. */

namespace ltmm
{
  /** @class single_thread ltmm/single_thread.hh <ltmm/single_thread.hh>
      @brief Utility class. 
      This class is used to lock to insure proper
      allocation of the singletons in the library.  
      @see @ref threads 
   */
  class single_thread
  {
  protected:
    /** The last error */
    std::string _error;
  public:
    /** Constructor. */
    single_thread() {}
    /** Destructor.*/
    virtual ~single_thread() {}
    /** Acquire this lock */
    void lock() {}
    /** Release this lock */
    void unlock() {}
    /** Set error message in thread specific storage */
    void set_error(const char* error) { _error = error; }
    /** Get the error message from thread specific storage. */
    const char* get_error() const { return _error.c_str(); }
    /** Wether this is multithreaded or not */
    enum { multi_threaded = 0 };
  };
#ifdef _REENTRANT
# ifdef __WINDOWS__
# include <windows.h>
  class multi_thread 
  {
  protected:
    /** The last error */
    std::string _error;
    /** The mutex */
    HANDLE _mutex;
  public:
    /** Constructor. */
    multi_thread();
    /** Destructor.*/
    virtual ~multi_thread() {}
    /** Acquire this lock */
    void lock() {}
    /** Release this lock */
    void unlock() {}
    /** Set error message in thread specific storage */
    void set_error(const char* error) { lock(); _error = error; unlock(); }
    /** Get the error message from thread specific storage. */
    const char* get_error() const { return _error.c_str(); }
    /** Wether this is multithreaded or not */
    enum { multi_threaded = 1 };
  };
  inline 
  multi_thread::multi_thread() 
    : _mutex(0)
  {
    _mutex = CreateMutex(0, FALSE, "LTMM Mutex");
    if (!_mutex) 
      throw std::runtime_error("multi_thread (WIN32) failed to get mutex");
  }
  inline 
  multi_thread::~multi_thread() 
  {
    if (!_mutex) return;
    BOOL ret = CloseHandle(_mutex);
    if (!ret) return;
    _mutex = 0;
  }
  inline 
  void 
  multi_thread::lock() 
  {
    DWORD ret = WaitForSingleObject(_mutex, INFINITY);
    switch (ret) {
    case WAIT_ABANDONED:
    case WAIT_OBJECT_O:
    case WAIT_TIMEOUT:
      break;
    }
  }
  inline 
  void 
  multi_thread::unlock() 
  {
    BOOL ret = ReleaseMutex(_mutex);
    if (ret) return;
    return;
  }
#elif HAS_PTHREAD_MUTEX_T
  class multi_thread 
  {
  protected:
    /** The last error */
    std::string _error;
    /** The mutex */
    pthread_mutex_t* _mutex;
  public:
    /** Constructor. */
    multi_thread();
    /** Destructor.*/
    virtual ~multi_thread() {}
    /** Acquire this lock */
    void lock() {}
    /** Release this lock */
    void unlock() {}
    /** Set error message in thread specific storage */
    void set_error(const char* error) { lock(); _error = error; unlock(); }
    /** Get the error message from thread specific storage. */
    const char* get_error() const { return _error.c_str(); }
    /** Wether this is multithreaded or not */
    enum { multi_threaded = 1 };
  };
  inline 
  multi_thread::multi_thread() 
    : _mutex(0)
  {
    pthread_mutex_init(_mutex, 0);
  }
  inline 
  multi_thread::~multi_thread() 
  {
    if (!_mutex) return;
    int ret = pthread_mutex_destoy(_mutex);
    switch (ret) {
    case EBUSY:   throw std::runtime_error("mutex is locked");
    }
  }
  inline 
  void 
  multi_thread::lock() 
  {
    int ret = pthread_mutex_lock(_mutex);
    switch (ret) {
    case EINVAL:  throw std::runtime_error("mutex not initizalised");
    case EDEADLK: throw std::runtime_error("lock would deadlock");
    }
  }
  inline 
  void 
  multi_thread::unlock() 
  {
    int ret = pthread_mutex_unlock(_mutex);
    switch (ret) {
    case EINVAL: throw std::runtime_error("mutex not initizalised");
    case EPERM:  throw std::runtime_error("mutex not held by thread");
    }
  }
# endif  // HAVE_PTHREAD_MUTEX_T
#endif  // _REENTRANT
}

#endif
//____________________________________________________________________
//
// EOF
//
