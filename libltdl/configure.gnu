#!/bin/sh
#
# $Id: configure.gnu,v 1.6 2005-01-06 02:45:13 cholm Exp $
#
#  libltdl.configure 
#  Copyright (C) 2003  Christian Holm <cholm@linux.HAL3000> 
#  
#  This library is free software; you can redistribute it and/or 
#  modify it under the terms of the GNU Lesser General Public 
#  License as published by the Free Software Foundation; either 
#  version 2.1 of the License, or (at your option) any later version. 
#  
#  This library is distributed in the hope that it will be useful, 
#  but WITHOUT ANY WARRANTY; without even the implied warranty of 
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
#  Lesser General Public License for more details. 
#  
#  You should have received a copy of the GNU Lesser General Public 
#  License along with this library; if not, write to the Free Software 
#  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  
#  USA   

args=
conf=`dirname $0`/`basename $0 .gnu`

while test $# -gt 0 ; do 
    case $1 in 
	--*)     args="$args $1" ;;
	*=*)     ;;
    esac
    shift 
done 

# echo "Running $conf $args"
# $conf $args

#
# EOF
#
