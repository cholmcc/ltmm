#!/usr/bin/make -f
# -*- mode: makefile -*- 
# $Id: autogen.sh,v 1.11 2005-06-27 11:05:55 cholm Exp $
#
#   Copyright (C) 2002  Christian Holm Christensen <cholm@nbi.dk>
#
#   This library is free software; you can redistribute it and/or
#   modify it under the terms of the GNU Lesser General Public License 
#   as published by the Free Software Foundation; either version 2 of 
#   the License, or (at your option) any later version.  
#
#   This library is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   Lesser General Public License for more details. 
#
#   You should have received a copy of the GNU Lesser General Public
#   License along with this library; if not, write to the Free
#   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
#   MA 02111-1307  USA  
#
CXX		= g++
CC		= gcc
PREFIX		= $(HOME)/tmp
CONFFLAGS	= --prefix=$(PREFIX) 
AUTOMAKEVERSION = 
PACKAGE		:= $(strip $(shell grep AC_INIT configure.ac |\
			sed 's/.*([^,]*,\([^,]*\),[^,]*,\(.*\)).*/\2/'))
VERSION		:= $(strip $(shell grep AC_INIT configure.ac |\
			sed 's/.*([^,]*,\([^,]*\),[^,]*,\(.*\)).*/\1/'))

all:	setup
	./configure --disable-optimization $(CONFFLAGS) 


setup: 	ChangeLog configure

config.guess config.sub libltdl/configure.ac ltmain.sh:configure.ac
	libtoolize --automake --ltdl --copy --force

aclocal.m4:configure.ac libltdl/configure.ac 
	aclocal$(AUTOMAKEVERSION) -I . -I support 

config/config.hh.in:aclocal.m4
	autoheader 

Makefile.in: config/config.hh.in aclocal.m4 configure.ac Makefile.am
	automake$(AUTOMAKEVERSION) --add-missing --copy 

configure:Makefile.in configure.ac aclocal.m4 
	autoconf 

make:	all
	$(MAKE) -f Makefile 

ChangeLog:
	rm -f $@
	touch $@
	rcs2log > $@

dists:  
	$(MAKE) dist
	$(MAKE) tar-ball -C doc


show:
	@echo "$(PACKAGE) version $(VERSION)"
clean: 
	find . -name Makefile.in 	| xargs rm -f 
	find . -name Makefile   	| xargs rm -f 
	find . -name "*~" 		| xargs rm -f 
	find . -name core 		| xargs rm -f 
	find . -name .libs 		| xargs rm -rf 
	find . -name .deps 		| xargs rm -rf 
	find . -name "*.lo" 		| xargs rm -f 
	find . -name "*.o" 		| xargs rm -f 
	find . -name "*.obj" 		| xargs rm -f 
	find . -name "*.ilk" 		| xargs rm -f 
	find . -name "*.exp" 		| xargs rm -f 
	find . -name "*.pdb" 		| xargs rm -f 
	find . -name "*.lib" 		| xargs rm -f 
	find . -name "*.la" 		| xargs rm -rf
	find . -name "*.log" 		| xargs rm -rf
	find . -name "TAGS" 		| xargs rm -rf

	rm -f 	config/missing       	\
	  	config/mkinstalldirs 	\
	  	config/ltmain.sh     	\
	  	config/config.guess 	\
		config/config.sub 	\
		config/depcomp		\
		config/install-sh 	\
		config/ltconfig 	\
		config/config.hh 	\
		config/config.hh.in 	\
		config/stamp-h		\
		config/stamp-h.in	\
		config/stamp-h1

	rm -rf 	aclocal.m4 		\
		autom4te.cache		\
		config.cache 		\
		config.status  		\
		config.log 		\
		configure 		\
		INSTALL 		\
		ChangeLog		\
		configure-stamp		\
		build-stamp		\
		libtool

	rm -f 	missing       		\
	  	mkinstalldirs 		\
	  	ltmain.sh     		\
	  	config.guess 		\
		config.sub 		\
		depcomp			\
		install-sh 		\
		ltconfig 		\
		config.hh 		\
		config.hh.in 		\
		stamp-h*		\
		libtool.m4		\
		ltdl.m4			\
		ltoptions.m4		\
		ltsugar.m4		\
		ltversion.m4		\
		semantic.cache	

	rm -rf  doc/$(PACKAGE).tags	\
		doc/doxyconfig		\
		doc/html		\
		doc/latex		\
		doc/man			

	rm -rf  tests/factory		\
		test/mybackend		\
		tests/simple		\
		tests/super		\
		tests/testsuite		\
		tests/testsuite.dir	\
		tests/package.m4	\
		tests/atconfig		

	rm -rf  *.tar.gz		\
		*.deb *.rpm		\
		$(PACKAGE)-?.?

	rm -f   libltdl/COPYING.LIB	\
		libltdl/Makefile	\
		libltdl/Makefile.am	\
		libltdl/Makefile.in	\
		libltdl/README		\
		libltdl/acinclude.m4	\
		libltdl/aclocal.m4	\
		libltdl/config-h.in	\
		libltdl/config.h	\
		libltdl/config.log	\
		libltdl/config.status	\
		libltdl/config.guess    \
		libltdl/config.sub      \
		libltdl/configure.ac    \
		libltdl/configure	\
		libltdl/configure.in	\
		libltdl/install-sh	\
		libltdl/libtool		\
		libltdl/ltdl.c		\
		libltdl/ltdl.h		\
		libltdl/ltmain.sh	\
		libltdl/missing		\
		libltdl/mkinstalldirs	\
		libltdl/stamp-h		\
		libltdl/stamp-h1	\
		libltdl/stamp-h.in	


#
# EOF
#
